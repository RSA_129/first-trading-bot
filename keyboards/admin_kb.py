from aiogram.types import ReplyKeyboardMarkup, KeyboardButton, InlineKeyboardButton, InlineKeyboardMarkup

#-------------- Update --------------
a = ['id', 'username', 'am_of_deps','on_account', 'active_dep', 'deposited', 'withdrawn',
    'language']
kb_upd = []
for i in a:
    kb_upd.append(KeyboardButton(i))
updMenu = ReplyKeyboardMarkup(resize_keyboard=True)
updMenu.add(*[i for i in kb_upd])

querries = ['set', '+', '-']
querries2 = []
for i in querries:
    querries2.append(KeyboardButton(i))
modeMenu = ReplyKeyboardMarkup(resize_keyboard=True).row(querries2[1], querries2[2]).add(querries2[0])


#-------------- Search --------------
s_b = ['id', 'username', 'am_of_deps','on_account', 'active_dep', 'deposited', 'withdrawn',
    'trader_profit' ,'language']
kb_search = []
for i in s_b[1:]:
    kb_search.append(KeyboardButton(i))
searchMenu = ReplyKeyboardMarkup(resize_keyboard=True)
searchMenu.add(*[i for i in kb_search])

#--------------- Choice --------------
choice = [KeyboardButton(i) for i in ['True', 'False', 'Pass']]
choiceMenu = ReplyKeyboardMarkup(resize_keyboard=True).row(*[i for i in choice]).insert('/stop')

#--------------- Send message --------------
send_message_kb = InlineKeyboardMarkup().insert(InlineKeyboardButton('✅', callback_data='send_message_confirm')). \
                                         insert(InlineKeyboardButton('❌', callback_data='send_message_decline'))
dist_message_kb = InlineKeyboardMarkup().insert(InlineKeyboardButton('✅', callback_data='dist_message_confirm')). \
                                         insert(InlineKeyboardButton('❌', callback_data='dist_message_decline'))

#--------------- Config ------------------
tables = ['config', 'config_photos', 'currencies', 'percentage_rate']
tablesMenu = ReplyKeyboardMarkup(resize_keyboard=True)
for i in tables[:3]:
    tablesMenu.insert(KeyboardButton(i))

async def idsMenu(*ids):
    _menu = ReplyKeyboardMarkup(resize_keyboard=True)
    for i in ids:
        _menu.insert(KeyboardButton(i))
    return _menu

config_change_value_kb = InlineKeyboardMarkup().insert(InlineKeyboardButton('✅', callback_data='config_confirm')). \
                                                insert(InlineKeyboardButton('❌', callback_data='config_decline'))

config_insert_curr_kb = InlineKeyboardMarkup().insert(InlineKeyboardButton('✅', callback_data='config_ins_con')). \
                                               insert(InlineKeyboardButton('❌', callback_data='config_ins_dec'))

ban_kb = InlineKeyboardMarkup().insert(InlineKeyboardButton('✅', callback_data='ban_con')). \
                                insert(InlineKeyboardButton('❌', callback_data='ban_dec'))