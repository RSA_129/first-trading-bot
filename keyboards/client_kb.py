from aiogram.types import ReplyKeyboardMarkup, KeyboardButton, InlineKeyboardButton, InlineKeyboardMarkup
from database import main_db, just_db

#-------------- FAQ --------------
b1 = KeyboardButton('1')
b2 = KeyboardButton('2')
b3 = KeyboardButton('3')
b4 = KeyboardButton('4')
b5 = KeyboardButton('5')

faqMenu = ReplyKeyboardMarkup(resize_keyboard=True)
faqMenu.row(b1, b2, b3, b4, b5).add(KeyboardButton('/main'))

#-------------- Invsetments --------------
async def currenciesMenu():
    arr_currencies = await just_db.sql_config_ids_get('currencies')
    arr_currencies = [KeyboardButton(i) for i in arr_currencies if i != 'usd']
    _dl = len(arr_currencies) // 2
    arr_currencies1 = arr_currencies[:_dl]
    arr_currencies2 = arr_currencies[_dl:]
    menu = ReplyKeyboardMarkup(resize_keyboard=True).row(*arr_currencies1).row(*arr_currencies2)
    return menu

invest_types_kb = InlineKeyboardMarkup().add(InlineKeyboardButton('Invest', callback_data='invest')).add(
                    InlineKeyboardButton('Deposits', callback_data='deposits')).insert(
                    InlineKeyboardButton('Reinvest', callback_data='reinvest')).row(
                    InlineKeyboardButton('Withdraw', callback_data='withdrawal'),
                    InlineKeyboardButton("Withrawl's history", callback_data='with_hist')).add(
                    InlineKeyboardButton('Affiliate program', callback_data='referals'))
invest_types_kb_new = InlineKeyboardMarkup().add(InlineKeyboardButton('Invest', callback_data='invest'))
request_send_kb = InlineKeyboardMarkup().add(InlineKeyboardButton('Sent', callback_data='request'))

invest_back_kb = ReplyKeyboardMarkup(resize_keyboard=True).row(KeyboardButton('/main'))

#-------------- Withdraw ----------------
async def withdraw_kb(id): #изменить, чтобы была проверка по всем депозитам (и которые кончились)
    withdraw_currencies = await main_db.sql_deposit_check_curr(id)
    withdrawMenu = ReplyKeyboardMarkup(resize_keyboard=True)
    for i in withdraw_currencies:
        withdrawMenu.insert(KeyboardButton(i.upper()))
    return withdrawMenu

withdraw_request_kb = InlineKeyboardMarkup().row(InlineKeyboardButton('✅', callback_data='withdraw_req_confirm'),
                                                 InlineKeyboardButton('❌', callback_data='withdraw_req_exit'))
#-------------- Reinvest ----------------
reinvest_request = InlineKeyboardMarkup().row(InlineKeyboardButton('✅', callback_data='reinvest_req_confirm'),
                                                 InlineKeyboardButton('❌', callback_data='reinvest_req_exit'))
#-------------- Affiliate program ----------------
ref_lvls_kb = InlineKeyboardMarkup().row(InlineKeyboardButton('1️⃣', callback_data='first_lvl'),
                                        InlineKeyboardButton('2️⃣', callback_data='second_lvl'),
                                        InlineKeyboardButton('3️⃣', callback_data='third_lvl'))

back_to_ac_kb = ReplyKeyboardMarkup(resize_keyboard=True).add(KeyboardButton('/account'))