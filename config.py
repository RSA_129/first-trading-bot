from datetime import datetime as dt, timedelta
from database import just_db

async def curr(name):
    foo = Currencies(name)
    await foo._init()
    return foo

class Currencies:
    def __init__(self, name):
        self.name = name
        self.currency = {}
        self.value = None

    async def _init(self):
        _c = await just_db.sql_config_values_get('currencies', 'name', self.name)
        if _c:
            for key, value in enumerate(_c):
                self.currency[just_db.currencies_coll_names[key]] = value

    def value_get(self, collumn):
        try:
            self.value = self.currency[collumn]
            return self.value
        except KeyError:
            return None
    def all(self):
        return self.currency

bsc_currs = ['eth', 'link', 'usdt', 'bnb']

deposits = [
    {'id': 0, 'min': 15, 'days': 12},
    {'id': 1, 'min': 100, 'days': 10},
    {'id': 2, 'min': 250, 'days': 8},
    {'id': 3, 'min': 500, 'days': 5} ]

ref_program = [5, 2, 1] # in %%

async def deps_percs(n_deposit):
    perc = await just_db.sql_percs_get()
    return perc[n_deposit]

async def check_amount(value, collumn):
    for i in deposits[::-1]:
        if value >= i['min']:
            return i[collumn]

time_add_to_utc = 3

async def tm(exact):
    if bool(exact):
        return dt.strftime(dt.utcnow(), '%y.%m.%d %H:%M:%S')
    else:
        return dt.strftime(dt.utcnow(), '%y.%m.%d %H:%M')

async def tm_est(exact):
    if bool(exact):
        return dt.strftime(dt.utcnow() - timedelta(hours=5), '%y.%m.%d %H:%M:%S')
    else:
        return dt.strftime(dt.utcnow() - timedelta(hours=5), '%y.%m.%d %H:%M')

referral_num = 78945612
deposit_num = [159715, 235749, 354116, 456699, 548962, 675135, 745821, 852223, 992158]
bot_link = 't.me/...'