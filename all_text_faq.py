from database import just_db
from all_text import command_account

async def text(num):
    if num == 1:
        return f"""
&#10145;&#65039; <b>How long have you been trading?</b>
— I have been trading a lot
...
"""
    if num == 2:
        return f"""
...
...
...
"""
    if num == 3:
        return f"""
&#10145;&#65039; <b>What cryptocurrencies do you accept?</b>
— You can find the list of currently supported cryptocurrencies in the "Invest" section.
&#10145;&#65039; <b>What is the minimum and maximum investment limit?</b>
— The minimum investment limit is $15. The maximum investment limit is ${await just_db.sql_config_values_get('config', 'name', 'max_to_invest', 'value')}.
"""
    if num == 4:
        return f"""
&#10145;&#65039; <b>How do you withdraw funds?</b>
— Go to the /{command_account} section and then click "Withdraw" button and follow the instructions.
&#10145;&#65039; <b>How fast are funds withdrawn?</b>
...
...
"""
    else:
        return f"""
&#10145;&#65039; <b>Do you have a referral program?</b>
— Yes, I have a multi-level referral program in the "Affiliate Program" section of the bot.
"""