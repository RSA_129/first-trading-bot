import asyncio

from create_bot import dp, bot
from aiogram import Bot, Dispatcher, types
from aiogram.dispatcher import DEFAULT_RATE_LIMIT
from aiogram.dispatcher.handler import CancelHandler, current_handler
from aiogram.dispatcher.middlewares import BaseMiddleware
from aiogram.utils import exceptions
from aiogram.utils.exceptions import Throttled
from aiogram.types import ParseMode, ReplyKeyboardRemove
import logging
from database import main_db
from id_admin import ID

logging.basicConfig(level=logging.INFO)
log = logging.getLogger('broadcast')

class ThrottlingMiddleware(BaseMiddleware):
    """
    Simple middleware
    """

    def __init__(self, limit=DEFAULT_RATE_LIMIT, key_prefix='antiflood_'):
        self.rate_limit = limit
        self.prefix = key_prefix
        super(ThrottlingMiddleware, self).__init__()

    async def on_process_message(self, message: types.Message, data: dict):
        """
        This handler is called when dispatcher receives a message

        :param message:
        """
        # Get current handler
        handler = current_handler.get()

        # Get dispatcher from context
        dispatcher = Dispatcher.get_current()
        # If handler was configured, get rate limit and key from handler
        if handler:
            limit = getattr(handler, 'throttling_rate_limit', self.rate_limit)
            key = getattr(handler, 'throttling_key', f"{self.prefix}_{handler.__name__}")
        else:
            limit = self.rate_limit
            key = f"{self.prefix}_message"

        # Use Dispatcher.throttle method.
        try:
            await dispatcher.throttle(key, rate=limit)
        except Throttled as t:
            # Execute action
            await self.message_throttled(message, t)

            # Cancel current handler
            raise CancelHandler()

    async def message_throttled(self, message: types.Message, throttled: Throttled):
        """
        Notify user only on first exceed and notify about unlocking only on last exceed

        :param message:
        :param throttled:
        """
        handler = current_handler.get()
        dispatcher = Dispatcher.get_current()
        if handler:
            key = getattr(handler, 'throttling_key', f"{self.prefix}_{handler.__name__}")
        else:
            key = f"{self.prefix}_message"

        # Calculate how many time is left till the block ends
        delta = throttled.rate - throttled.delta

        # Prevent flooding
        if throttled.exceeded_count <= 2:
            await send_message(message.from_user.id, 'Too many requests!')

        # Sleep.
        await asyncio.sleep(delta)

        # Check lock status
        thr = await dispatcher.check_key(key)

        # If current message is not last with current key - do not send message
        if thr.exceeded_count == throttled.exceeded_count:
            pass

class AccessMiddleware(BaseMiddleware):
    def __init__(self):
        super().__init__()

    async def on_process_message(self, message: types.Message, _):
        if message.from_user.id in await main_db.sql_banned_get():
            # await message.answer(txt_banned_message)
            raise CancelHandler()
        elif message.text:
            if len(message.text) > 200:
                if message.from_user.id not in ID:
                    await send_message(message.from_user.id, 'Too long message.', None)
                    raise CancelHandler()
                else:
                    if len(message.text) > 3950:
                        await message.answer('Too long message.')
                        raise CancelHandler()
    async def on_process_callback_query(self, callback: types.CallbackQuery, _):
        if callback.from_user.id in await main_db.sql_banned_get():
            # await bot.send_message(callback.from_user.id, txt_banned_message)
            raise CancelHandler()

async def send_message(user_id, text=None, reply_markup = ReplyKeyboardRemove(), pagepreview=True, photo=None, file=None,
                       return_msg_id = None):
    try:
        if photo:
            msg = await bot.send_photo(user_id, photo, text, parse_mode=ParseMode.HTML, reply_markup=reply_markup)
        elif file:
            msg = await bot.send_document(user_id, file, caption=text, parse_mode=ParseMode.HTML, reply_markup=reply_markup)
        else:
            msg = await bot.send_message(user_id, text, parse_mode=ParseMode.HTML, reply_markup=reply_markup,
                                   disable_web_page_preview=pagepreview)
    except exceptions.BotBlocked:
        log.error(f"Target [ID:{user_id}]: blocked by user")
    except exceptions.ChatNotFound:
        log.error(f"Target [ID:{user_id}]: invalid user ID")
    except exceptions.RetryAfter as e:
        log.error(f"Target [ID:{user_id}]: Flood limit is exceeded. Sleep {e.timeout} seconds.")
        await asyncio.sleep(e.timeout)
        return await send_message(user_id, text, reply_markup, pagepreview, photo, file)
    except exceptions.UserDeactivated:
        log.error(f"Target [ID:{user_id}]: user is deactivated")
    except exceptions.TelegramAPIError:
        log.exception(f"Target [ID:{user_id}]: failed")
    else:
        log.info(f"Target [ID:{user_id}]: success")
        if return_msg_id:
            return msg.message_id
        else:
            return True
    return False

async def dist_messages(text, photo, *users):
    try:
        count = 0
        for user_id in list(users):
            if await send_message(user_id, text, photo=photo):
                count += 1
            await asyncio.sleep(.05)  # 20 messages per second (Limit: 30 messages per second)
    finally:
        log.info(f"{count} messages successful sent.")
    return count