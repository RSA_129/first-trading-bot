from database import main_db, just_db
from config import deposits, check_amount, bot_link, referral_num, curr
from datetime import datetime as dt, timedelta

txt_unkown_comm = 'Unknown command.\n/help to see all commands.'

async def to_emoji(num):
    try:
        num = list(str(int(num)))
        arr_of_emojis = ['0&#xFE0F;&#x20E3;', '1&#xFE0F;&#x20E3;', '2&#xFE0F;&#x20E3;', '3&#xFE0F;&#x20E3;',
                         '4&#xFE0F;&#x20E3;', '5&#xFE0F;&#x20E3;', '6&#xFE0F;&#x20E3;', '7&#xFE0F;&#x20E3;',
                         '8&#xFE0F;&#x20E3;', '9&#xFE0F;&#x20E3;']
        nw = []
        for value in num:
            nw.append(arr_of_emojis[int(value)])
        return ''.join(nw)
    except:
        return num

bot_name = 'Example trade bot'
bot_name_short = 'ETB'
command_account = 'account'
command_switch_notif = 'notify'
comm_rates = 'rates'

async def get_admin_id():
    return await just_db.sql_config_values_get('config', 'name', 'admin_id', 'value')
async def txt_admin():
    return f"You can contact me through the {await get_admin_id()}"

# txt_banned_message = 'U have been banned, ask admin if u think this is an error'

async def txt_amount_of_users(num):
    if num:
        return f"""&#x1F465;Number of active users: {await to_emoji(num)}
<i>Updated at</i> {(dt.utcnow() - timedelta(hours=5)).date()} 00:00 EST"""
    return """There are currently Error active deposits"""

txt_onstart_1 = f"""
&#128200<b>{bot_name}</b> is very functional crypto bot!

&#128640Through my trading abilities I will bring you money!!!
"""

txt_onstart_2 = f'''
&#9881<i><b>Commands:</b></i>
How it works - /how
Account - /{command_account} or /ac 
FAQ - /faq
Rates - /{comm_rates}
'''

txt_hiw = """
Im kinda provide roudmap and statement, so belive that I can help you to increase your capital...
"""

async def txt_rate():
    keys = await just_db.sql_config_ids_get('currencies')
    keys = keys[1:]
    rates = []
    for i in keys:
        rates.append(await just_db.sql_config_values_get('currencies', 'name', i, 'rate'))
    keys = [i.upper() for i in keys]
    msg = []
    _start = '&#x1f4b1; <b><i>Cryptocurrency Exchange Rates</i></b>:'
    msg.append(_start)
    for i, value in enumerate(keys):
        msg.append(f'1 <i>{value}</i> = {rates[i]} <i>USD</i>')
    msg.append(f'<i>Updated at</i> {(dt.utcnow() - timedelta(hours=5)).date()} 00:00 EST')
    return '\n'.join(msg)

txt_faq = """
<b>1.</b> General questions
<b>2.</b> Registration
<b>3.</b> Investment
<b>4.</b> Withdraw
<b>5.</b> Affilite program
"""

txt_more_than_on_acc = f"<i>Too few</i> assets on the account for the transaction.\n/{command_account} to quit"
txt_less_than_min = f"<i>Too few</i> assets for the transaction.\n/{command_account} to quit"
txt_inform_amount_plans = 'Plan will be chosen automatically depending on what amount you will invest'
txt_inform_store_money = '<i>Note that deposits and withdrawals are in cryptocurrency, and storage on the account is held in dollars.</i>'

async def txt_invest_start(new, id):
    if new:
        if await main_db.sql_request_select(id):
            return 'Wait for your request to be handled.'
        return "Sorry, but you have not invested anything yet.\nPress this button to invest or type /main to exit."
    else:
        req = ''
        req_user = await main_db.sql_request_select(id)
        if req_user:
            if req_user[7] == 'deposit':
                req = "\n<i>You've sent request for investing. Wait for it to be handled.</i>"
            else:
                req = "\n<i>You've sent request for withdrawing. Wait for it to be handled.</i>"
        _refs = await main_db.sql_referral_search(id, 'profit_from_1', 'profit_from_2', 'profit_from_3')
        if _refs:
            _refs = round(sum(_refs), 2)
        else:
            _refs = 0
        return f"""
<b>&#128212;Here're your details:</b>
On account: <b>${await main_db.sql_find(id, 'on_account')}</b>
Amount of active deposits: <b>{await main_db.sql_find(id, 'am_of_deps')}</b>
Active deposits: <b>${await main_db.sql_find(id, 'active_dep')}</b>
Withdrawn: <b>${await main_db.sql_find(id, 'withdrawn')}</b>
Profit from referrals: <b>${_refs}</b>
{req}
"""

async def txt_capped_amount():
    _ = await just_db.sql_config_values_get('config', 'name', 'max_to_invest', 'value')
    return f'''Type amount in chosen cryptocurrency.
<i>Total deposited funds are capped at ${_}</i>.

<i>{txt_inform_amount_plans}</i>.
'''
# текст, если уже есть запрос на что-то
txt_unwanted_req = 'You have already sent request.\nWait for it to be handled.'

# тексты при отправке запроса на депозит

def txt_wallet(wallet):
    return f'''Send <b>exactly</b> this amount to <code>{wallet}</code>.\nOnly after this press "<i><b>sent</b></i>" button.\n/{command_account} to quit
\n<i>I proceed transaction after the 1st confirmation from the Crypto network.</i>'''
def txt_wallet_bsc(wallet):
    return f'''Send <b>exactly</b> this amount to <code>{wallet}</code>.\nOnly after this press "<i><b>sent</b></i>" button.\n/{command_account} to quit
\n<i>I proceed transaction after the 1st confirmation from the Crypto network. \nPay attention that I accept this cryptocurrency only in Binance Smart Chain (BEP-20).</i>'''
def txt_min_value(currency, value):
    return f"Minimum value for {currency} is {value}."
def txt_max_value(currency, in_crypto):
    return f'The maximum amount left to invest is approximately {in_crypto} {currency.upper()}.'
def txt_invest_amount(amount, curr):
    return f'Your amount is <b>{amount} {curr.upper()}</b>.'

txt_invest_error = 'Transaction error, try again later or change the last digit of the amount.'

def txt_reinvest(amount):
    return f"Would you like to reinvest <b>${amount}</b>?"

txt_reinv_req_with_er = 'Sorry, but once the withdrawal request is confirmed there will not be enough funds on your account for reinvestment.'

#текст при выводе при вводе суммы
async def txt_withdraw_amount(id, commision, curr):
    on_acc = await main_db.sql_find(id, 'on_account')
    deposited = await main_db.sql_find(id, 'deposited')
    if commision:
        def set_max_value(on_acc):
            def add_comm(num):
                commision = round(int(num * 0.05 * 100) / 100, 2)
                total = round(num + commision, 2)
                return total
            txt = round(on_acc*100/105, 2)
            mx_input = add_comm(txt)
            if on_acc != mx_input:
                back_txt = txt
                txt = round(round(txt * 100 + 1) / 100, 2)
                mx_input = add_comm(txt)
                if on_acc == mx_input:
                    return txt
                elif on_acc < mx_input:
                    return back_txt
                else:
                    print('Error in all_text.txt_withdraw_amount')
                    return -1
            else:
                return txt
        total_txt = f"""Considering the commision <b>${set_max_value(on_acc)}</b> is available to withdraw.
Invest <b>${round(deposits[1]['min']-deposited, 2)}</b> from /{command_account} or more to withdraw without commision.
To withdraw anyway type amount in <b><i>USD</i></b>:"""
        return total_txt
    else:
        return f"""<b>${await main_db.sql_find(id, 'on_account')}</b> are available to withdraw.
Type amount in <b><i>USD</i></b>:"""

txt_with_bsc = '<i>Pay attention that I send this cryptocurrency only in Binance Smart Chain (BEP-20).</i>'

async def txt_approve_req(type, amount_in_usd=None, amount_crypt=None, curr=None, uni_id = None):
    if type == 'deposit':
        return f"""
Now you have new deposit with unique number <b><i>{uni_id}</i></b> and amount of <b>${amount_in_usd}</b>.
Check it in 'deposits' through /{command_account}"""
    if type == 'withdraw':
        return f"Your funds in amount of <b>{amount_crypt} {curr.upper()}</b> have been withdrawn.\nPlease, check your wallet."

async def txt_decline_req(type):
    return f"""Your {type} request has been declined.\nContact {await get_admin_id()} if there was an error."""

async def txt_deposit_from_refs(amount, uniq_id):
    return f"You've got <b>${amount}</b> for referral's deposit with uniq num = <b><i>{uniq_id}</i></b>."

def txt_accrual(acc_value, dep_am):
    return f"You've got <b>${acc_value}</b> on your account from <b>${dep_am}</b> deposit."
def txt_end_dep(dep_am):
    return f"Your deposit amount of <b>${dep_am}</b> has expired. Funds was returned to account."

# текс при выводе депов
async def txt_deps(id):
    deps = []
    for dep in (await main_db.sql_deposit_user_to_dep_id(id))[::-1][0:10]:
        _time = dt.strptime(dep[2], '%y.%m.%d %H:%M') - timedelta(hours=5)
        _time_start_form = _time.strftime('%y/%m/%d %H:%M')
        _days_to_add = await check_amount(dep[-1], 'days')
        _time_end_form = (_time + timedelta(days=_days_to_add)).strftime('%y/%m/%d %H:%M')
        deps.append(f'''
&#10145;&#65039;Start time: {_time_start_form} EST
End time: {_time_end_form} EST
Amount: <b>${dep[-1]}</b>
Plan: {(just_db.plan_names[(await check_amount(dep[-1], 'id'))]).capitalize()}
''')
    dlina = len(deps)
    if dlina == 1:
        deps.insert(0, "Here's your last deposit:\n")
    else:
        deps.insert(0, f"Here're your last <b><i>{dlina}</i></b> deposits:\n")
    return ''.join(deps)

# текст для истории выводов
async def txt_with_hist(id):
    withs = []
    for wit in (await main_db.sql_with_get(id))[::-1][0:10]:
        _time = dt.strptime(wit[1], '%y.%m.%d %H:%M') - timedelta(hours=5)
        _time_form = _time.strftime('%y/%m/%d %H:%M')
        withs.append(f"""
&#10145;&#65039;Time: {_time_form} EST
Amount: <b>{wit[3]} {wit[2].upper()}</b>
To: <i>{wit[-1]}</i>
""")
    dlina = len(withs)
    if dlina == 1:
        withs.insert(0, f"Here's your last withdrawal:\n")
    elif dlina == 0:
        withs.append(f'You have no withdrawals.')
    else:
        withs.insert(0, f"Here're your last <b><i>{dlina}</i></b> withdrawals:\n")
    return ''.join(withs)

# текс для реф. проги
async def txt_refs(id, lvl):
    titles = ['First', 'Second', 'Third']
    title = titles[lvl-1]
    am = await main_db.sql_referral_search(id, f'amount_of_{lvl}')
    profit = await main_db.sql_referral_search(id, f'profit_from_{lvl}')
    return f"""
&#x1F465;<b>{title} Level Referrals</b>

Amount: <b>{am}</b>
Profit: <b>${round(profit, 2)}</b>
"""

txt_ref_the_same_page = 'Yor are already on this page'

async def txt_ref_link(id):
    raw = f'{bot_link}?start={id+referral_num}'
    return f'🔗Your referral link is {raw}'

# текст при выводе вставке валют в конфиге
async def txt_check_insertion(*args):
    _start = 'Check values:\n'
    _colls = just_db.currencies_coll_names
    _txt = []
    for key, value in enumerate(args):
        _txt.append(f'{_colls[key]} = {value}')
    _txt = '\n'.join(_txt)
    return _start + _txt