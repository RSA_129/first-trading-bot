import sqlite3 as sq

plan_names = ['basic', 'standart', 'optimal', 'agressive']
currencies_coll_names = ['№', 'name', 'name_to_parc', 'rate', 'min', 'round_acc', 'wallet']
def sql_start_2():
    global base, cur
    base = sq.connect('database/users_2.db')
    cur = base.cursor()
    base.execute("""CREATE TABLE IF NOT EXISTS logs(time text, id integer, event text, amount real, other text)""")
    base.execute("""CREATE TABLE IF NOT EXISTS percentage_rate(id integer PRIMARY KEY, plan text, amount real)""")
    base.execute("""CREATE TABLE IF NOT EXISTS config(name text PRIMARY KEY, value real)""")
    base.execute("""CREATE TABLE IF NOT EXISTS config_photos(place text PRIMARY KEY, id text)""")
    base.execute("""CREATE TABLE IF NOT EXISTS currencies(№ integer PRIMARY KEY, name text, name_to_parc text, 
                        rate real, min real, round_acc integer, wallet text)""")

    inserts = [
        ('INSERT INTO percentage_rate VALUES (?,?,?),(?,?,?),(?,?,?),(?,?,?)', (0, plan_names[0], 1.1,
                           1, plan_names[1], 1.5, 2, plan_names[2], 2.5, 3, plan_names[3], 3)),
        ('INSERT INTO config VALUES (?,?),(?,?),(?,?)', ('am_of_users', None,
                                    'admin_id', None, 'max_to_invest', 150)),
        ('INSERT INTO config_photos VALUES (?,?),(?,?),(?,?),(?,?),(?,?),(?,?),(?,?)', ('start', None, 'statement', None,
                            'plans', None, 'ref_prog', None, 'road_map', None, 'faq', None, 'account', None)),
        ('INSERT INTO currencies VALUES (?,?,?,?,?,?,?)', (0, 'usd', None, 1, 15, 2, None))
    ]
    for i in inserts:
        try:
            cur.execute(*i)
        except sq.IntegrityError:
            pass
    base.commit()
    if base:
        print('Database 2 connected')

def sql_close_2():
    cur.close()
    base.close()

############################################## LOGGING ###########################################
async def sql_logs_add(*args):
    cur.execute('INSERT INTO logs VALUES (?,?,?,?,?)', args)
    base.commit()

async def sql_logs_reqs_count(id):
    decls = cur.execute('select id from logs where id == ? and (event == ? or event == ?)',
                                        (id,'decline_dep_cr', 'decline_with')).fetchall()
    trushniki = cur.execute('select id from logs where id == ? and (event == ? or event == ?)',
                                        (id,'confirm_dep_cr', 'confirm_with')).fetchall()
    arr = []
    for i in [trushniki, decls]:
        if not i:
            arr.append(0)
        else:
            arr.append(len(i))
    return arr
############################################## Percentage Rate ###########################################
async def sql_percs_get():
    search = cur.execute('select amount from percentage_rate ORDER BY id').fetchall()
    search = [i[0] for i in search]
    return search

async def sql_perc_change(plan, value):
    cur.execute('update percentage_rate set amount == ? where plan == ?', (value, plan))
    base.commit()

############################################## Config ###########################################
async def sql_config_ids_get(table_name):
    if table_name == 'currencies':
        search = cur.execute(f'select * from {table_name} order by №').fetchall()
        search = [str(i[1]) for i in search]
    else:
        search = cur.execute(f'select * from {table_name}').fetchall()
        search = [str(i[0]) for i in search]
    return search

async def sql_config_values_get_by_id(table_name, id):
    if table_name == 'config':
        collumn = 'name'
    elif table_name == 'config_photos':
        collumn = 'place'
    elif table_name == 'currencies':
        collumn = 'name'
    else:
        collumn = 'id'
    search = cur.execute(f'select * from {table_name} where {collumn} == ?', (id,)).fetchone()
    return search

async def sql_config_update(table_name, collumn, id, value):
    if table_name == 'config':
        collumn_id = 'name'
    elif table_name == 'config_photos':
        collumn_id = 'place'
    elif table_name == 'currencies':
        collumn_id = 'name'
    else:
        collumn_id = 'id'
    try:
        cur.execute(f'update {table_name} set {collumn} == ? where {collumn_id} == ?', (value, id))
        base.commit()
        return True
    except Exception as e:
        print(e)
        return False

async def sql_config_values_get(table_name, *args): # collumn_with_id, id, collumn
    if len(args) == 0:
        search = cur.execute(f'select * from {table_name}').fetchall()
    elif len(args) == 2:
        search = cur.execute(f'select * from {table_name} where {args[0]} == ?', (args[1],)).fetchone()
    elif len(args) == 3:
        search = cur.execute(f'select {args[2]} from {table_name} where {args[0]} == ?', (args[1],)).fetchone()[0]
    else:
        return False
    return search

async def sql_config_add_curr(*args):
    try:
        args = tuple([None if i.lower() == 'none' else i for i in args])
        cur.execute('INSERT INTO currencies VALUES (?,?,?,?,?,?,?)', args)
        base.commit()
        return True
    except sq.IntegrityError:
        return False
