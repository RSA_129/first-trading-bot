import sqlite3 as sq
from keyboards.admin_kb import a

def sql_start():
    global base, cur
    base = sq.connect('database/users.db')
    cur = base.cursor()
    base.execute("""CREATE TABLE IF NOT EXISTS main(id INTEGER PRIMARY KEY, username TEXT, am_of_deps INTEGER, 
                on_account REAL, active_dep REAL, deposited REAL, withdrawn REAL, 
                trader_profit REAL, language TEXT)""")
    base.execute("""CREATE TABLE IF NOT EXISTS requests(id INTEGER PRIMARY KEY, username TEXT, language TEXT,
                time TEXT, currency TEXT, amount REAL, commission REAL, op_type TEXT, wallet REAL, state TEXT)""")
    base.execute("""CREATE TABLE IF NOT EXISTS users(id INTEGER PRIMARY KEY, invited_by INTEGER, 
                    username TEXT, banned INTEGER, notif_refs INTEGER)""")
    base.execute("""CREATE TABLE IF NOT EXISTS deposits(dep_id INTEGER PRIMARY KEY AUTOINCREMENT, user_id INTEGER, 
                start_time TEXT, days_left INTEGER, currency TEXT, amount_in_usd REAL)""")
    base.execute("""CREATE TABLE IF NOT EXISTS withdrawals(user_id INTEGER, time TEXT, currency TEXT, amount REAL, 
                                    wallet text)""")
    base.execute("""CREATE TABLE IF NOT EXISTS ref_program(id INTEGER PRIMARY KEY, amount_of_1 INTEGER, profit_from_1 REAL, 
                                    amount_of_2 INTEGER, profit_from_2 REAL, amount_of_3 INTEGER, profit_from_3 REAL)""")
    base.commit()
    if base:
        print('Database 1 connected')


def sql_close_1():
    cur.close()
    base.close()

async def sql_add(state):
    async with state.proxy() as data:
        cur.execute('INSERT INTO main VALUES (?,?,?,?,?,?,?,?,?)', tuple(data.values()))
        base.commit()
async def sql_main_add(*args):
    cur.execute('INSERT INTO main VALUES (?,?,?,?,?,?,?,?,?)', args)
    base.commit()
async def sql_update(identificator, collumn, value):
    try:
        value = round(value, 2)
    except TypeError:
        pass
    cur.execute(f'update main set {collumn} == ? where id == ?', (value, identificator))
    base.commit()
async def sql_find(value, *args): #collumn to print
    if len(args) == 1: # value to find and collumn
        search = cur.execute(f'select {args[0]} from main where id == ?', (value,)).fetchone()[0]
        try:
            search = round(search, 2)
        except TypeError:
            pass
    else: # value to find
        search = cur.execute('select * from main where id == ?', (value,)).fetchone()
    return search
async def sql_amount_active():
    amount = cur.execute('select count(*) from main').fetchone()[0]
    return amount

async def sql_add_value(id, collumn, value):
    previous_value = await sql_find(id, collumn)
    value = round(previous_value + value, 2)
    cur.execute(f'update main set {collumn} == ? where id == ?', (value, id))
    base.commit()

###################### REQUESTS ############################
async def sql_add_request(id, un, lan, time, curr, am, com, op_type, wallet, state):
    cur.execute('INSERT INTO requests VALUES (?,?,?,?,?,?,?,?,?,?)', (id, un, lan, time, curr,
                                                        am, com, op_type, wallet, state))
    base.commit()

async def sql_preremove_request(id, state):
    cur.execute('update requests set state == ? where id == ?', (state, id))
    base.commit()
async def sql_remove_request(id):
    cur.execute('delete from requests where id == ?', (id,))
    base.commit()
async def sql_remove_requests_by_state(state):
    cur.execute('delete from requests where state == ?', (state,))
    base.commit()
async def sql_request_select(id):
    search = cur.execute(f'select * from requests where id == ?', (id,)).fetchone()
    return search
async def sql_request_select_all(op_type, state):
    if op_type == '*':
        search = cur.execute(f'select * from requests where state == ? ORDER BY time',(state,)).fetchall()
    else:
        search = cur.execute(f'select * from requests where op_type == ? and state == ? ORDER BY time',
                             (op_type, state)).fetchall()
    return search
async def sql_request_undo(id):
    cur.execute('update requests set state == 0 where id == ?', (id,))
    base.commit()
async def sql_req_check_am(currency, value, wallet):
    search = cur.execute('select * from requests where currency == ? and amount == ? and op_type == ? and wallet == ?',
                         (currency, value, 'deposit', wallet)).fetchone()
    return search
###################### DEPOPOSITS ############################
async def sql_deposit_add(*args): #dep_id, user_id, time, days_left, currency, amount
    cur.execute('INSERT INTO deposits(user_id, start_time, days_left, currency, amount_in_usd) VALUES (?,?,?,?,?)',
                        args)
    base.commit()
async def sql_deposit_get_values(dep_id):
    search = cur.execute('select * from deposits where dep_id == ?', (dep_id,)).fetchone()
    return search
async def sql_update_days_by_one(dep_id):
    current_days = await sql_deposit_get_values(dep_id)
    current_days = current_days[3]
    cur.execute(f'update deposits set days_left == ? where dep_id == ?', (current_days - 1,dep_id))
    base.commit()
    return current_days - 1
async def sql_deposit_check_curr(user_id):
    search = cur.execute("select currency from deposits where user_id == ? and currency != 'usd'", (user_id,)).fetchall()
    search = set([i[0] for i in search])
    return search
async def sql_deposit_ids():
    search = cur.execute('select dep_id from deposits where days_left != 0 order by dep_id').fetchall()
    search = [i[0] for i in search]
    return search
async def sql_deposit_user_to_dep_id(user_id):
    search = cur.execute('select * from deposits where user_id == ?', (user_id,)).fetchall()
    return search

async def sql_deposit_get_last_dep_id():
    search = cur.execute('select dep_id from deposits order by dep_id desc').fetchone()
    if search:
        search = search[0]
    else:
        search = 0
    return search

###################### Users ############################
async def sql_user_add(id, invited_by, un):
    try:
        cur.execute('INSERT INTO users VALUES (?,?,?,?,?)', (id, invited_by, un, None, None))
        base.commit()
        return True
    except sq.IntegrityError:
        return False
async def sql_user_search(id):
    search = cur.execute('select * from users where id == ?', (id,)).fetchone()
    return search
async def sql_users_get():
    search = cur.execute('select id from users').fetchall()
    search = [i[0] for i in search]
    return search
async def sql_banned_get():
    search = cur.execute('select id from users where banned == 1').fetchall()
    search = [i[0] for i in search]
    return search
async def sql_bl_no_get():
    search = cur.execute('select id from users where notif_refs == 1').fetchall()
    search = [i[0] for i in search]
    return search
async def sql_user_update(id, collumn, value=None):
    if not value:
        prev_value = cur.execute(f'select {collumn} from users where id == ?', (id,)).fetchone()[0]
        if prev_value:
            cur.execute(f'update users set {collumn} == ? where id == ?', (None, id))
        else:
            cur.execute(f'update users set {collumn} == ? where id == ?', (1, id))
    else:
        cur.execute(f'update users set {collumn} == ? where id == ?', (value, id))
    base.commit()
###################### Withdrawals ############################
async def sql_with_add(*args):
    cur.execute('INSERT INTO withdrawals VALUES (?,?,?,?,?)', args)
    base.commit()
async def sql_with_get(user_id):
    search = cur.execute('select * from withdrawals where user_id == ?', (user_id,)).fetchall()
    return search
###################### Referral program ############################
async def sql_referral_add(id):
    cur.execute('INSERT INTO ref_program VALUES (?,?,?,?,?,?,?)', (id, 0, 0, 0, 0, 0, 0))
    base.commit()
async def sql_referral_search(id, *args):
    if len(args) == 0:
        search = cur.execute('select * from ref_program where id == ?', (id,)).fetchone()
        return search
    else:
        search = cur.execute(f'select {", ".join(args)} from ref_program where id == ?', (id,)).fetchone()
    if search:
        if len(search) == 1:
            return search[0]
        else:
            return search
    return None

async def sql_referral_add_value(id, collumn, value):
    previous_value = await sql_referral_search(id, collumn)
    value = previous_value + value
    cur.execute(f'update ref_program set {collumn} == ? where id == ?', (value, id))
    base.commit()