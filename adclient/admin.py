from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters.state import State, StatesGroup
from aiogram import types, Dispatcher
from create_bot import dp, bot
from database import main_db
from aiogram.types import ReplyKeyboardRemove
from id_admin import ID
from message_handler import send_message
from all_text import txt_unkown_comm
from keyboards.admin_kb import searchMenu, updMenu, modeMenu, s_b
from aiogram.types import ParseMode
from config import referral_num

# Cancel
async def cancel(message: types.Message, state: FSMContext):
    if message.from_user.id in ID:
        cur_state = await state.get_state()
        if not cur_state:
            return
        await state.finish()
        await send_message(message.from_user.id, 'Cancel happened.')
    else:
        await send_message(message.from_user.id, txt_unkown_comm, None)

def register_adclient_admin_cancel(dp: Dispatcher):
    dp.register_message_handler(cancel, commands=['cancel'], state="*")


###################### UPDATE ###########################
class FSMAdmin_update(StatesGroup):
    identificator = State()
    collumn = State()
    mode = State()
    value = State()

#Start, check
async def cm_start_upd(message: types.Message):
    if message.from_user.id in ID:
        await FSMAdmin_update.identificator.set()
        await message.answer('What user to change')
    else:
        await send_message(message.from_user.id, txt_unkown_comm, None)
# choose id
async def upd_identificator(message: types.Message, state: FSMContext):
    try:
        async with state.proxy() as data_up:
            data_up['identificator'] = int(message.text)
        if await main_db.sql_find(data_up['identificator']):
            await FSMAdmin_update.next()
            await message.answer('Type collumn', reply_markup=updMenu)
        else:
            await message.answer('There is no such id\nType valid')
    except ValueError:
        await message.answer('Invalid number')
# choose collumn
async def upd_collumn(message: types.Message, state: FSMContext):
    async with state.proxy() as data_up:
        data_up['collumn'] = message.text
    if data_up['collumn'] in s_b: # check if it exists
        await FSMAdmin_update.next()
        if data_up['collumn'] in ['am_of_deps', 'on_account', 'active_dep', 'deposited', 'withdrawn']:
            await message.answer(f'''Current value in {data_up['collumn']}: {await main_db.sql_find(
                                data_up['identificator'],
                                data_up['collumn'])}\nChoose mode''', reply_markup=modeMenu)
        else:
            async with state.proxy() as data_up:
                data_up['mode'] = 'set'
            await message.answer(f'''Current value in {data_up['collumn']}: {await main_db.sql_find(
                                data_up['identificator'],
                                data_up['collumn'])}\nType value''', reply_markup=ReplyKeyboardRemove())
            await FSMAdmin_update.next()
    else:
        await message.answer('Wrong name of collumn')
# set, + or -
async def upd_mode(message: types.Message, state: FSMContext):
    async with state.proxy() as data_up:
        data_up['mode'] = message.text
    if data_up['mode'] in ['set', '+', '-']: # check if mode is right
        await FSMAdmin_update.next()
        await message.answer('Type value', reply_markup=ReplyKeyboardRemove())
    else:
        await message.answer('Wrong mode. Type right mode')
# type value
async def upd_value(message: types.Message, state: FSMContext):
    async with state.proxy() as data_up:
        try:
            data_up['value'] = int(message.text)
        except:
            try:
                data_up['value'] = float(message.text)
            except:
                data_up['value'] = message.text
        try:
            if data_up['mode'] != 'set':
                if data_up['mode'] == '+':
                    data_up['value'] += await main_db.sql_find(data_up['identificator'],
                                        data_up['collumn'])
                else:
                    data_up['value'] = await main_db.sql_find(data_up['identificator'],
                                        data_up['collumn']) - data_up['value']
            await main_db.sql_update(data_up['identificator'], data_up['collumn'], data_up['value'])

            if data_up['collumn'] in ['deposited', 'withdrawn']:
                data_up['profit'] = await main_db.sql_find(data_up['identificator'],
                                'deposited') - await main_db.sql_find(data_up['identificator'],
                                'withdrawn')
                await main_db.sql_update(data_up['identificator'], 'trader_profit', data_up['profit'])

            elif data_up['collumn'] == 'id':
                await main_db.sql_update(data_up['value'], 'referral_number',
                                    data_up['value'] + referral_num)
            await message.answer('Done', reply_markup=ReplyKeyboardRemove())
            await state.finish()
        except TypeError:
            await message.answer('Not valid number')

def register_adclient_admin_update(dp: Dispatcher):
    dp.register_message_handler(cm_start_upd, commands=['update'], state=None)
    dp.register_message_handler(upd_identificator, state=FSMAdmin_update.identificator)
    dp.register_message_handler(upd_collumn, state=FSMAdmin_update.collumn)
    dp.register_message_handler(upd_mode, state=FSMAdmin_update.mode)
    dp.register_message_handler(upd_value, state=FSMAdmin_update.value)


###################### SEARCH ###########################
class FSMAdmin_search(StatesGroup):
    identificator = State()
    collumn = State()

#Start, check
async def cm_start_search(message: types.Message):
    if message.from_user.id in ID:
        await FSMAdmin_search.identificator.set()
        await message.answer('Who')
    else:
        await send_message(message.from_user.id, txt_unkown_comm, None)
# choose id
repl = {}
async def search_identificator(message: types.Message, state: FSMContext):
    try:
        async with state.proxy() as data_search:
            data_search['identificator'] = int(message.text)
        fu = await main_db.sql_find(data_search['identificator'])
        if fu:
            for i in range(len(s_b)):
                repl[s_b[i]] = fu[i]
            repl2 = [f'<b>{key}</b>: {repl[key]}' for key in repl]
            await message.answer('\n'.join(repl2), parse_mode=ParseMode.HTML)
            await message.answer('Choose collumn or /cancel', reply_markup=searchMenu)
            await FSMAdmin_search.next()
        else:
            await message.answer(f'There is no user with id {message.text}')
    except ValueError:
        await message.answer('Invalid number')
async def search_choose_col(message: types.Message, state: FSMContext):
    try:
        async with state.proxy() as data_search:
            data_search['identificator'] = message.text
        await message.answer(repl[data_search['identificator']], reply_markup=ReplyKeyboardRemove())
        await state.finish()
    except:
        await message.answer('Wrong input')


def register_adclient_admin_search(dp: Dispatcher):
    dp.register_message_handler(cm_start_search, commands=['search'], state=None)
    dp.register_message_handler(search_identificator, state=FSMAdmin_search.identificator)
    dp.register_message_handler(search_choose_col, state=FSMAdmin_search.collumn)