from aiogram import types, Dispatcher
from create_bot import dp
from aiogram.types.message import ContentTypes
from aiogram.dispatcher import FSMContext
from message_handler import send_message
from all_text import txt_unkown_comm

async def other_send(message : types.Message, state: FSMContext):
    await send_message(message.from_user.id, txt_unkown_comm, None)

    
def register_adclient_other(dp : Dispatcher):
    dp.register_message_handler(other_send, content_types=ContentTypes.ANY, state='*')