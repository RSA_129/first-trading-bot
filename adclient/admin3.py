from create_bot import dp, bot
from aiogram import types, Dispatcher
from aiogram.dispatcher.filters.state import State, StatesGroup
from aiogram.dispatcher import FSMContext
from id_admin import ID
from keyboards.admin_kb import send_message_kb, dist_message_kb, ban_kb
from message_handler import send_message, dist_messages
from database import main_db
from all_text import txt_unkown_comm

class FSMsend_mess(StatesGroup):
    text = State()
    text_to_all = State()
    ban = State()

######################################## Message to 1 person ########################################
async def send_message_start(message: types.Message, state: FSMContext):
    if message.from_user.id in ID:
        if await state.get_state():
            await state.finish()
        try:
            async with state.proxy() as data_sendm:
                data_sendm['user_id'] = int(message.get_args())
            await FSMsend_mess.text.set()
            await send_message(message.from_user.id, 'Type message')
        except ValueError:
            await send_message(message.from_user.id, 'Invalid id')
    else:
        await send_message(message.from_user.id, txt_unkown_comm, None)

async def send_message_text(message: types.Message, state: FSMContext):
    prefix = '<b>You got new message from admin:</b>\n\n'
    async with state.proxy() as data_sendm:
        data_sendm['text'] = prefix+message.text
    await send_message(message.from_user.id, f'R u sure u want to send this message to {data_sendm["user_id"]}:')
    await send_message(message.from_user.id, data_sendm['text'], send_message_kb)

async def send_message_confirm(callback: types.CallbackQuery, state: FSMContext):
    async with state.proxy() as data_sendm:
        if await send_message(data_sendm['user_id'], data_sendm['text']):
            await send_message(callback.from_user.id, 'Message has been sent')
        else:
            await send_message(callback.from_user.id, 'Some error occured')
    await bot.edit_message_reply_markup(callback.from_user.id, message_id=callback.message.message_id,
                                        reply_markup=None)
    await state.finish()

async def send_message_decline(callback: types.CallbackQuery, state: FSMContext):
    await state.finish()
    await send_message(callback.from_user.id, 'Exit done')
    await bot.edit_message_reply_markup(callback.from_user.id, message_id=callback.message.message_id,
                                        reply_markup=None)

######################################## Distribution messages ############################################
async def dist_messages_start(message: types.Message, state: FSMContext):
    if message.from_user.id in ID:
        if await state.get_state():
            await state.finish()
        await send_message(message.from_user.id, 'Type text')
        await FSMsend_mess.text_to_all.set()
    else:
        await send_message(message.from_user.id, txt_unkown_comm, None)

async def dist_messages_text(message: types.Message, state: FSMContext):
    async with state.proxy() as data_distm:
        if message.text:
            data_distm['text'] = message.text
            data_distm['photo'] = None
        else:
            data_distm['photo'] = message.photo[-1].file_id
            data_distm['text'] = message.caption
    await send_message(message.from_user.id, 'R u su re u want to send this message to all users')
    await send_message(message.from_user.id, data_distm['text'], dist_message_kb, photo=data_distm['photo'])

async def dist_message_confirm(callback: types.CallbackQuery, state: FSMContext):
    async with state.proxy() as data_distm:
        users = await main_db.sql_users_get()
        if await dist_messages(data_distm['text'], data_distm['photo'], *users):
            await send_message(callback.from_user.id, 'Messages have been sent')
        else:
            await send_message(callback.from_user.id, 'Some error occured')
    await bot.edit_message_reply_markup(callback.from_user.id, message_id=callback.message.message_id,
                                        reply_markup=None)
    await state.finish()

async def dist_message_decline(callback: types.CallbackQuery, state: FSMContext):
    await state.finish()
    await send_message(callback.from_user.id, 'Exit done')
    await bot.edit_message_reply_markup(callback.from_user.id, message_id=callback.message.message_id,
                                        reply_markup=None)

######################################## Ban ############################################
async def ban(message: types.Message, state: FSMContext):
    user_id = message.from_user.id
    if user_id in ID:
        if await state.get_state():
            await state.finish()
        args = message.get_args().split()
        if args:
            if await main_db.sql_user_search(args[0]):
                if len(args) == 1:
                    await FSMsend_mess.ban.set()
                    async with state.proxy() as data:
                        data['id'] = args[0]
                    await send_message(user_id, f'Ban {user_id}?', ban_kb)
                else:
                    await FSMsend_mess.ban.set()
                    async with state.proxy() as data:
                        data['id'] = args[0]
                        data['text'] = ' '.join(args[1:])
                    await send_message(user_id, f"""Ban {user_id} with message to him:\n"{data['text']}" """, ban_kb)
            else:
                await send_message(user_id, 'There is no such user in db')
        else:
            await send_message(user_id, 'Type command in such pattern:\nban [id] (text)')
    else:
        await send_message(message.from_user.id, txt_unkown_comm, None)

async def ban_confirm(callback: types.CallbackQuery, state: FSMContext):
    async with state.proxy() as data:
        await main_db.sql_user_update(data['id'], 'banned')
        try:
            data['text']
            await send_message(data['id'], data['text'])
        except KeyError:
            pass
        await send_message(callback.from_user.id, f"User {data['id']} has been (un-)banned")
    await state.finish()
    await bot.edit_message_reply_markup(callback.from_user.id, message_id=callback.message.message_id,
                                        reply_markup=None)

async def ban_decline(callback: types.CallbackQuery, state: FSMContext):
    await state.finish()
    await send_message(callback.from_user.id, 'Exit done')
    await bot.edit_message_reply_markup(callback.from_user.id, message_id=callback.message.message_id,
                                        reply_markup=None)

def register_adclient_admin3(dp: Dispatcher):
    dp.register_message_handler(send_message_start, commands='send', state=None)
    dp.register_message_handler(send_message_text, state=FSMsend_mess.text)
    dp.register_callback_query_handler(send_message_confirm, state=FSMsend_mess.text, text='send_message_confirm')
    dp.register_callback_query_handler(send_message_decline, state=FSMsend_mess.text, text='send_message_decline')

    dp.register_message_handler(dist_messages_start, commands='dist', state=None)
    dp.register_message_handler(dist_messages_text, content_types=['text', 'photo'], state=FSMsend_mess.text_to_all)
    dp.register_callback_query_handler(dist_message_confirm,  state=FSMsend_mess.text_to_all, text='dist_message_confirm')
    dp.register_callback_query_handler(dist_message_decline, state=FSMsend_mess.text_to_all, text='dist_message_decline')

    dp.register_message_handler(ban, commands='ban', state='*')
    dp.register_callback_query_handler(ban_confirm, state=FSMsend_mess.ban, text='ban_con')
    dp.register_callback_query_handler(ban_decline, state=FSMsend_mess.ban, text='ban_dec')