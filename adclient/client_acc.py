import asyncio
import sqlite3

from aiogram import types, Dispatcher
from create_bot import dp, bot
from aiogram.dispatcher.filters.state import State, StatesGroup
from aiogram.dispatcher import FSMContext
from aiogram.utils.exceptions import MessageNotModified
from database import main_db, just_db
from all_text import txt_invest_start, txt_wallet, txt_min_value, command_account, txt_unwanted_req, \
txt_withdraw_amount, txt_reinvest, txt_invest_amount, txt_invest_error, txt_max_value, txt_capped_amount, \
txt_more_than_on_acc, txt_less_than_min, txt_inform_amount_plans, txt_wallet_bsc, txt_inform_store_money, \
txt_reinv_req_with_er, txt_with_bsc
from keyboards.client_kb import invest_types_kb, invest_types_kb_new, currenciesMenu, request_send_kb, \
withdraw_kb, withdraw_request_kb, reinvest_request, invest_back_kb
from database import main_db
import time
from config import check_amount, deposits, tm, curr, bsc_currs
from message_handler import send_message

class User_account(StatesGroup):
    currency = State()
    invest_amount = State()
    wallet = State()
    send = State()
    reinvest = State()
    reinvest_amount = State()
    withdraw_currency = State()
    withdraw_amount = State()
    withdraw_wallet = State()
    withdraw_confirm = State()

async def main(message: types.Message, state: FSMContext):
    if await state.get_state():
        await state.finish()
    file_id = await just_db.sql_config_values_get('config_photos', 'place', 'account', 'id')
    if not await main_db.sql_find(message.from_user.id): # если не нашлось, то значит еще не инвестировал
        _txt = await txt_invest_start(True, message.from_user.id)
        if file_id.startswith('A'):
            await send_message(message.from_user.id, _txt, invest_types_kb_new, photo=file_id)
        else:
            await send_message(message.from_user.id, _txt, invest_types_kb_new, file=file_id)
    else:
        _txt = await txt_invest_start(False, message.from_user.id)
        if file_id.startswith('A'):
            await send_message(message.from_user.id, _txt, invest_back_kb, photo=file_id)
        else:
            await send_message(message.from_user.id, _txt, invest_back_kb, file=file_id)
        await send_message(message.from_user.id, '<i>Choose what to do:</i>', invest_types_kb)

async def invest(callback: types.CallbackQuery, state: FSMContext):
    if await state.get_state():
        await state.finish()
    if await main_db.sql_request_select(callback.from_user.id):
        await send_message(callback.from_user.id, txt_unwanted_req, None)
    else:
        file_id = await just_db.sql_config_values_get('config_photos', 'place', 'plans', 'id')
        _txt = 'Choose what currency to invest with'
        if not await main_db.sql_find(callback.from_user.id):
            await send_message(callback.from_user.id, txt_inform_store_money)
        if file_id.startswith('A'):
            await send_message(callback.from_user.id, _txt, await currenciesMenu(), photo=file_id)
        else:
            await send_message(callback.from_user.id, _txt, await currenciesMenu(), file=file_id)
        await User_account.currency.set()
    await callback.answer()

async def currency(message: types.Message, state: FSMContext):
    async with state.proxy() as data_inv:
        data_inv['currency'] = message.text

    if data_inv['currency'] in (await just_db.sql_config_ids_get('currencies'))[1:]:
        await send_message(message.from_user.id, await txt_capped_amount())
        await User_account.invest_amount.set()
    else:
        await send_message(message.from_user.id, f'Wrong name of currency.\nType right or /{command_account}', None)

async def invest_amount(message: types.Message, state: FSMContext):
    try:
        async with state.proxy() as data_inv:
            cls_cur = await curr(data_inv['currency'])
            data_inv['amount'] = round(float(message.text.replace(',', '.')), cls_cur.value_get('round_acc'))
        _min = cls_cur.value_get('min')
        if _min <= data_inv['amount']:
            _table_max = await just_db.sql_config_values_get('config', 'name', 'max_to_invest', 'value')
            if await main_db.sql_find(message.from_user.id):
                _max = round(_table_max - await main_db.sql_find(message.from_user.id, 'deposited'), 2)
            else:
                _max = _table_max
            if _max >= data_inv['amount'] * cls_cur.value_get('rate'):
                _am_to_str = str(data_inv["amount"])
                _zeros_amount = _am_to_str + str(0) * (cls_cur.value_get('round_acc') - len(_am_to_str.split('.')[1]))
                _wallet = cls_cur.value_get('wallet')
                await send_message(message.from_user.id, txt_invest_amount(_zeros_amount, data_inv['currency']))
                if await main_db.sql_req_check_am(data_inv['currency'], data_inv['amount'], _wallet):
                    await send_message(message.from_user.id, txt_invest_error)
                else:
                    async with state.proxy() as data_inv:
                        data_inv['our_wallet'] = _wallet
                    if data_inv['currency'] in bsc_currs:
                        await send_message(message.from_user.id, txt_wallet_bsc(_wallet), request_send_kb)
                    else:
                        await send_message(message.from_user.id, txt_wallet(_wallet), request_send_kb)
            else:
                _in_crypt = round(_max/cls_cur.value_get('rate'), cls_cur.value_get('round_acc'))
                _in_crypt = "{:.9f}".format(_in_crypt).rstrip('0')
                if _in_crypt.endswith('.'):
                    _in_crypt = _in_crypt[:-1]
                await send_message(message.from_user.id, txt_max_value(data_inv['currency'], _in_crypt))
        else:
            await send_message(message.from_user.id, txt_min_value(data_inv['currency'], _min))
    except ValueError:
        await send_message(message.from_user.id, f'Invalid number.\n/{command_account} ot quit')

async def invest_send(callback: types.CallbackQuery, state: FSMContext):
    try:
        await bot.edit_message_reply_markup(callback.from_user.id, message_id=callback.message.message_id,
                                            reply_markup=None)
    except MessageNotModified:
        pass
    else:
        async with state.proxy() as data_inv:
            data_inv['id'] = callback.from_user.id
            if callback.from_user.username:
                data_inv['un'] = "@" + callback.from_user.username
            else:
                data_inv['un'] = None
            data_inv['time'] = await tm(0)
            data_inv['lan'] = callback.from_user.language_code
        try:
            await main_db.sql_add_request(data_inv['id'], data_inv['un'], data_inv['lan'], data_inv['time'],
                                          data_inv['currency'],  data_inv['amount'], "0", 'deposit',
                                          data_inv['our_wallet'], '0')
            await just_db.sql_logs_add(await tm(1), data_inv['id'], 'req_to_cr_dep',
                                       data_inv['amount']*(await curr(data_inv['currency'])).value_get('rate'),
                                       f"in crypto: {data_inv['amount']} {data_inv['currency']}")
            await send_message(callback.from_user.id, '<i>Request was sent.</i>')
            if await main_db.sql_find(callback.from_user.id):
                await asyncio.sleep(1)
                await main(callback, state)
        except sqlite3.IntegrityError:
            await send_message(message.from_user.id, txt_unwanted_req)
        await state.finish()
    finally:
        await callback.answer()

async def reinvest(callback: types.CallbackQuery, state: FSMContext):
    if await state.get_state():
        await state.finish()
    _min = (await curr('usd')).value_get('min')
    on_acc = await main_db.sql_find(callback.from_user.id, 'on_account')
    if _min > on_acc:
        await send_message(callback.from_user.id, txt_more_than_on_acc, None)
    else:
        _req = await main_db.sql_request_select(callback.from_user.id)
        if not _req:
            _req = [0 for i in range(10)]
            rate = 0
        else:
            if _req[7] == 'withdrawal':
                rate = (await curr(_req[4])).value_get('rate')
            else:
                _req = [0 for i in range(10)]
                rate = 0
        if _min > round(on_acc - _req[5]*rate - _req[6], 2):
            await send_message(callback.from_user.id, txt_reinv_req_with_er, None)
        else:
            async with state.proxy() as data_reinv:
                data_reinv['max_to_invest'] = round(on_acc - _req[5]*rate - _req[6], 2)
            await User_account.reinvest_amount.set()
            file_id = await just_db.sql_config_values_get('config_photos', 'place', 'plans', 'id')
            _txt = f'Type amount in <b><i>USD</i></b> to reinvest.\n\n<i>{txt_inform_amount_plans}.</i>'
            if file_id.startswith('A'):
                await send_message(callback.from_user.id, _txt, photo=file_id)
            else:
                await send_message(callback.from_user.id, _txt, file=file_id)
    await callback.answer()

async def reinvest_amount(message: types.Message, state: FSMContext):
    try:
        async with state.proxy() as data_reinv:
            data_reinv['amount'] = round(float(message.text.replace(',', '.')), 2)
        if data_reinv['amount'] > data_reinv['max_to_invest']:
            await send_message(message.from_user.id, txt_reinv_req_with_er)
        elif data_reinv['amount'] < (await curr('usd')).value_get('min'):
            await send_message(message.from_user.id, txt_less_than_min)
        else:
            await send_message(message.from_user.id, txt_reinvest(data_reinv['amount']), reinvest_request)
    except ValueError:
        await send_message(message.from_user.id, f'Invalid number.\n/{command_account} to quit')

async def reinvest_confirm(callback: types.CallbackQuery, state: FSMContext):
    try:
        await bot.edit_message_reply_markup(callback.from_user.id, message_id=callback.message.message_id,
                                            reply_markup=None)
    except MessageNotModified:
        pass
    else:
        async with state.proxy() as data_reinv:
            await main_db.sql_add_value(callback.from_user.id, 'on_account', -data_reinv['amount']) #-funds from
            await main_db.sql_add_value(callback.from_user.id, 'am_of_deps', 1) #+1 deposit
            await main_db.sql_add_value(callback.from_user.id, 'active_dep', data_reinv['amount']) # active deposits
            await main_db.sql_deposit_add(callback.from_user.id, await tm(0),
                                          await check_amount(data_reinv['amount'], 'days'),
                                          'usd', data_reinv['amount'])
            await just_db.sql_logs_add(await tm(1), callback.from_user.id, 'reinvest', data_reinv['amount'], None)
            await send_message(callback.from_user.id, '<i>Reinvestment done.</i>')
            await asyncio.sleep(1)
            await main(callback, state)
            await state.finish()
    finally:
        await callback.answer()

async def reinvest_req_exit(callback: types.CallbackQuery, state: FSMContext):
    try:
        await bot.edit_message_reply_markup(callback.from_user.id, message_id=callback.message.message_id,
                                            reply_markup=None)
    except MessageNotModified:
        pass
    else:
        await state.finish()
        await send_message(callback.from_user.id, '<i>Operation canceled.</i>')
        await asyncio.sleep(1)
        await main(callback, state)
    finally:
        await callback.answer()

async def withdraw(callback: types.CallbackQuery, state: FSMContext):
    if await state.get_state():
        await state.finish()
    if await main_db.sql_request_select(callback.from_user.id):
        await send_message(callback.from_user.id, txt_unwanted_req, None)
    elif (await curr('usd')).value_get('min') > await main_db.sql_find(callback.from_user.id, 'on_account'):
        await send_message(callback.from_user.id, 'There is not enough money on account to withdraw.', None)
    else:
        await send_message(callback.from_user.id, 'Choose currency:\n\n<i>Withdrawal will be proceed in chosen cryptocurrency.</i>', await withdraw_kb(callback.from_user.id))
        await User_account.withdraw_currency.set()
    await callback.answer()

async def withdraw_currency(message: types.Message, state: FSMContext):
    async with state.proxy() as data_withdr:
        data_withdr['currency'] = message.text.lower()
        data_withdr['commission'] = await check_withdrawn_commision(message.from_user.id)
    if data_withdr['currency'] in await main_db.sql_deposit_check_curr(message.from_user.id):
        if data_withdr['currency'] in bsc_currs:
            await send_message(message.from_user.id, txt_with_bsc)
        await send_message(message.from_user.id, await txt_withdraw_amount(message.from_user.id,
                                                        data_withdr['commission'], data_withdr['currency']))
        await User_account.withdraw_amount.set()
    else:
        await send_message(message.from_user.id, f'Wrong name of currency.\nType right or /{command_account}', None)

async def withdraw_amount(message: types.Message, state: FSMContext):
    try:
        async with state.proxy() as data_withdr:
            data_withdr['amount'] = round(float(message.text.replace(',', '.')), 2)
            data_withdr['commission_absol'] = round(int(data_withdr['amount'] * data_withdr['commission']*100)/100, 2)
        _total_am = round(data_withdr['amount'] + data_withdr['commission_absol'], 2)
        if _total_am > await main_db.sql_find(message.from_user.id, 'on_account'):
            await send_message(message.from_user.id, txt_more_than_on_acc)
        elif _total_am < deposits[0]['min']:
            await send_message(message.from_user.id, txt_less_than_min)
        else:
            await User_account.withdraw_wallet.set()
            await send_message(message.from_user.id, 'Type wallet:')
    except ValueError:
        await send_message(message.from_user.id, f'Invalid number.\n/{command_account} to quit')

async def withdraw_wallet(message: types.Message, state: FSMContext):
    async with state.proxy() as data_withdr:
        data_withdr['wallet'] = message.text
        data_withdr['am_in_crypto'] = round(data_withdr['amount']/(await curr(data_withdr['currency'])).value_get('rate'),
                                            (await curr(data_withdr['currency'])).value_get('round_acc'))
    await send_message(message.from_user.id,f"""
&#128499;Cheeeeeeck your withdraw details:
Amount: ${data_withdr['amount']} = {data_withdr['am_in_crypto']} {data_withdr['currency'].upper()}
Wallet number: {data_withdr['wallet']}
Commision: ${data_withdr['commission_absol']}""", withdraw_request_kb)

async def check_withdrawn_commision(id, currency=None):
    deposited = await main_db.sql_find(id, 'deposited')
    if deposited >= deposits[1]['min']:
        return 0
    else:
        return 0.05

async def withdraw_confirm(callback: types.CallbackQuery, state: FSMContext):
    try:
        await bot.edit_message_reply_markup(callback.from_user.id, message_id=callback.message.message_id,
                                            reply_markup=None)
    except MessageNotModified:
        pass
    else:
        try:
            async with state.proxy() as data_withdr:
                un = callback.from_user.username
                if un:
                    un = '@' + un
                await main_db.sql_add_request(callback.from_user.id, un, callback.from_user.language_code,
                                              await tm(0), data_withdr['currency'], data_withdr['am_in_crypto'],
                                              data_withdr['commission_absol'], 'withdrawal', data_withdr['wallet'], '0')
            await just_db.sql_logs_add(await tm(1), callback.from_user.id, 'req_to_with', data_withdr['amount'],
                                       f"in crypto: {data_withdr['am_in_crypto']} {data_withdr['currency']}")
            await send_message(callback.from_user.id, '<i>Request was sent.</i>')
            await asyncio.sleep(1)
            await main(callback, state)
        except sqlite3.IntegrityError:
            await send_message(callback.from_user.id, txt_unwanted_req)
        finally:
            await callback.answer()
    finally:
        await state.finish()

async def withdraw_req_exit(callback: types.CallbackQuery, state: FSMContext):
    try:
        await bot.edit_message_reply_markup(callback.from_user.id, message_id=callback.message.message_id,
                                            reply_markup=None)
    except MessageNotModified:
        pass
    else:
        await send_message(callback.from_user.id, '<i>Operation canceled.</i>')
        await asyncio.sleep(1)
        await main(callback, state)
        await callback.answer()
    await state.finish()

def register_adclient_client_invest(dp: Dispatcher):
    dp.register_message_handler(main, state='*', commands=[command_account, 'ac'])
    dp.register_callback_query_handler(invest, text='invest', state='*')
    dp.register_message_handler(currency, state=User_account.currency)
    dp.register_message_handler(invest_amount, state=User_account.invest_amount)
    dp.register_callback_query_handler(callback=invest_send, text='request', state=User_account.invest_amount)

    dp.register_callback_query_handler(callback=reinvest, text='reinvest', state='*')
    dp.register_message_handler(reinvest_amount, state=User_account.reinvest_amount)
    dp.register_callback_query_handler(callback=reinvest_confirm, text='reinvest_req_confirm', state=User_account.reinvest_amount)
    dp.register_callback_query_handler(callback=reinvest_req_exit, text='reinvest_req_exit', state=User_account.reinvest_amount)

    dp.register_callback_query_handler(callback=withdraw, text='withdrawal', state='*')
    dp.register_message_handler(withdraw_currency, state=User_account.withdraw_currency)
    dp.register_message_handler(withdraw_amount, state=User_account.withdraw_amount)
    dp.register_message_handler(withdraw_wallet, state=User_account.withdraw_wallet)
    dp.register_callback_query_handler(callback=withdraw_confirm, text='withdraw_req_confirm', state=User_account.withdraw_wallet)
    dp.register_callback_query_handler(callback=withdraw_req_exit, text='withdraw_req_exit', state=User_account.withdraw_wallet)