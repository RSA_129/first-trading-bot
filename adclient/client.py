import time

import aiogram.utils.exceptions
from aiogram import types, Dispatcher
from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters.state import State, StatesGroup
from create_bot import dp, bot
from keyboards.client_kb import faqMenu
from all_text import txt_onstart_1, txt_onstart_2, txt_faq, txt_hiw, txt_amount_of_users, txt_rate, \
txt_admin
import all_text_faq
from database import main_db, just_db
from aiogram.types import ParseMode
from config import referral_num, tm
from message_handler import send_message

class Faqstates(StatesGroup):
    choose = State()

async def comm_start(message : types.Message, state: FSMContext):
    if await state.get_state():
        await state.finish()
    upline_id = message.get_args()
    user_id = message.from_user.id
    if message.from_user.username:
        un = '@' + message.from_user.username
    else:
        un = None
    if not await main_db.sql_user_search(user_id):
        if upline_id.isdigit():
            upline_id = int(upline_id) - referral_num
            if await main_db.sql_find(upline_id): # if upline has deposit
                await main_db.sql_user_add(user_id, upline_id, un)
                await add_ref(upline_id)
                await just_db.sql_logs_add(await tm(1), user_id, 'new_user', None, f'upline={upline_id}')
            else:
                await main_db.sql_user_add(user_id, None, un)
                await just_db.sql_logs_add(await tm(1), user_id, 'new_user', None, None)
        else:
            await main_db.sql_user_add(user_id, None, un)
            await just_db.sql_logs_add(await tm(1), user_id, 'new_user', None, None)
        print('New user', message.from_user.values)
    file_id = await just_db.sql_config_values_get('config_photos', 'place', 'start', 'id')
    if file_id:
        if file_id.startswith('A'):
            await send_message(message.from_user.id, txt_onstart_1, photo=file_id)
        else:
            await send_message(message.from_user.id, txt_onstart_1, file=file_id)
    else:
        await send_message(message.from_user.id, txt_onstart_1)
    await send_message(user_id, txt_onstart_2)

async def comm_main(message: types.Message, state: FSMContext):
    if await state.get_state():
        await state.finish()
    await send_message(message.from_user.id, await txt_amount_of_users(await just_db.sql_config_values_get('config',
                                                            'name', 'am_of_users', 'value')))
    await send_message(message.from_user.id, txt_onstart_2)
    await send_message(message.from_user.id, await txt_admin())

async def comm_help(message: types.Message, state: FSMContext):
    if await state.get_state():
        await state.finish()
    await send_message(message.from_user.id, txt_onstart_2)

async def comm_hiw(message: types.Message, state: FSMContext):
    if await state.get_state():
        await state.finish()
    file_id = await just_db.sql_config_values_get('config_photos', 'place', 'statement', 'id')
    if file_id.startswith('A'):
        await send_message(message.from_user.id, txt_hiw, photo=file_id)
    else:
        await send_message(message.from_user.id, txt_hiw, file=file_id)
    file_id = await just_db.sql_config_values_get('config_photos', 'place', 'road_map', 'id')
    if file_id.startswith('A'):
        await send_message(message.from_user.id, photo=file_id)
    else:
        await send_message(message.from_user.id, file=file_id)

async def comm_rates(message: types.Message, state: FSMContext):
    if await state.get_state():
        await state.finish()
    await send_message(message.from_user.id, await txt_rate())

async def comm_faq(message: types.Message, state: FSMContext):
    if await state.get_state():
        await state.finish()
    file_id = await just_db.sql_config_values_get('config_photos', 'place', 'faq', 'id')
    if file_id.startswith('A'):
        await send_message(message.from_user.id, txt_faq, faqMenu, photo=file_id)
    else:
        await send_message(message.from_user.id, txt_faq, faqMenu, file=file_id)
    async with state.proxy() as data:
        data['msg_del'] = None
        data['msg_change'] = None
    await Faqstates.choose.set()

async def faq_choose(message: types.Message, state: FSMContext):
    user_id = message.from_user.id
    txt = message.text
    if txt in ['1', '2', '3', '4', '5']:
        async with state.proxy() as data:
            txt = int(txt)
            from_faq = await all_text_faq.text(txt)
            if data['msg_change']:
                try:
                    await bot.edit_message_text(from_faq, message.chat.id, data['msg_change'],
                                                parse_mode=ParseMode.HTML)
                    await bot.delete_message(message.chat.id, data['msg_del'])
                    data['msg_del'] = message.message_id
                except aiogram.utils.exceptions.MessageNotModified:
                    await bot.delete_message(message.chat.id, message.message_id)
                except aiogram.utils.exceptions.MessageCantBeEdited:
                    await send_message(user_id, 'Some error occured.')
                    print('Some error occured in faq.')
                    await state.finish()
                except aiogram.utils.exceptions.MessageToDeleteNotFound:
                    try:
                        await bot.edit_message_text(from_faq, message.chat.id, data['msg_change'],
                                                parse_mode=ParseMode.HTML)
                    except aiogram.utils.exceptions.MessageNotModified:
                        pass
            else:
                sent_msg_id = await send_message(user_id, from_faq, None, return_msg_id=True)
                data['msg_change'] = sent_msg_id
                data['msg_del'] = message.message_id
    else:
        await send_message(user_id, 'Choose number or /main', None)
        async with state.proxy() as data:
            data['msg_del'] = None
            data['msg_change'] = None


def register_adclient_client(dp: Dispatcher):
    dp.register_message_handler(comm_start, commands=['start'], state='*')
    dp.register_message_handler(comm_main, commands='main', state='*')
    dp.register_message_handler(comm_help, commands='help', state='*')
    dp.register_message_handler(comm_hiw, commands='how', state='*')
    dp.register_message_handler(comm_rates, commands='rates', state='*')
    dp.register_message_handler(comm_faq, commands='faq', state='*')
    dp.register_message_handler(faq_choose, state=Faqstates.choose)

async def add_ref(upline_id):
    if not await main_db.sql_referral_search(upline_id):
        await main_db.sql_referral_add(upline_id)
    await main_db.sql_referral_add_value(upline_id, 'amount_of_1', 1)
    upline2 = (await main_db.sql_user_search(upline_id))[1]
    if upline2:
        await main_db.sql_referral_add_value(upline2, 'amount_of_2', 1)
        up_line3 = (await main_db.sql_user_search(upline2))[1]
        if up_line3:
            await main_db.sql_referral_add_value(up_line3, 'amount_of_3', 1)