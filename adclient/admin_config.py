from create_bot import dp, bot
from aiogram import types, Dispatcher
from aiogram.dispatcher.filters.state import State, StatesGroup
from aiogram.dispatcher import FSMContext
from message_handler import send_message
from database import just_db
from id_admin import ID
from all_text import txt_check_insertion, txt_unkown_comm
from keyboards.admin_kb import tables, tablesMenu, idsMenu, config_change_value_kb, config_insert_curr_kb

class FSM_config(StatesGroup):
    table = State()
    identificator = State()
    collumn = State()
    value = State()
    photo = State()
    check = State()
    insert = State()

async def config_start(message: types.Message, state: FSMContext):
    if message.from_user.id in ID:
        if await state.get_state():
            await state.finish()
        await FSM_config.table.set()
        await send_message(message.from_user.id, 'Choose table', tablesMenu)
    else:
        await send_message(message.from_user.id, txt_unkown_comm, None)

async def config_table(message: types.Message, state: FSMContext):
    input_txt = message.text
    if input_txt in tables:
        async with state.proxy() as data_config:
            data_config['table'] = input_txt
        await FSM_config.identificator.set()
        ids = await just_db.sql_config_ids_get(data_config['table'])
        await send_message(message.from_user.id, 'Type identificator', await idsMenu(*ids))
    else:
        await send_message(message.from_user.id, 'Invalid table name', None)

async def config_identificator(message: types.Message, state: FSMContext):
    async with state.proxy() as data_config:
        pass
    input_txt = message.text
    if input_txt in await just_db.sql_config_ids_get(data_config['table']):
        async with state.proxy() as data_config:
            data_config['identificator'] = input_txt
        _values = await just_db.sql_config_values_get_by_id(data_config['table'], input_txt)
        await send_message(message.from_user.id, f"Here's current values with id = {input_txt} from {data_config['table']}:\n{_values}")
        if data_config['table'] in ['currencies', 'percentage_rate']:
            await send_message(message.from_user.id, 'Type collumn name')
            await FSM_config.collumn.set()
        else:
            if data_config['table'] == 'config_photos':
                await FSM_config.photo.set()
                async with state.proxy() as data_config:
                    data_config['collumn'] = 'id'
                await send_message(message.from_user.id, 'Upload a photo')
            else:
                await FSM_config.value.set()
                async with state.proxy() as data_config:
                    data_config['collumn'] = 'value'
                await send_message(message.from_user.id, 'Type value')
    else:
        await send_message(message.from_user.id, 'Wrong id', None)

async def config_collumn(message: types.Message, state: FSMContext):
    async with state.proxy() as data_config:
        data_config['collumn'] = message.text.lower()
    if data_config['table'] == 'config_photos':
        await FSM_config.photo.set()
        await send_message(message.from_user.id, 'Upload a photo')
    else:
        await FSM_config.value.set()
        await send_message(message.from_user.id, 'Type value')

async def config_value(message: types.Message, state: FSMContext):
    async with state.proxy() as data_config:
        data_config['value'] = message.text.replace(',', '.')
    await config_check(message, state)

async def config_photo(message: types.Message, state: FSMContext):
    async with state.proxy() as data_config:
        try:
            data_config['value'] = message.photo[-1].file_id
        except (AttributeError, IndexError):
            data_config['value'] = message.document.file_id
    await send_message(message.from_user.id, 'Check all data')
    await config_check(message, state)

async def config_check(message: types.Message, state: FSMContext):
    async with state.proxy() as data_config:
        if data_config['table'] == 'currencies':
            await send_message(message.from_user.id, f"""
Change <b>{data_config['collumn']}</b> of <b>{data_config['identificator']}</b> from <b>{data_config['table']}</b> to 
<i>{data_config['value']}</i>?
""", config_change_value_kb)
        else:
            await send_message(message.from_user.id, f"""
Change <b>{data_config['identificator']}</b> from <b>{data_config['table']}</b> to 
<i>{data_config['value']}</i>?
""", config_change_value_kb)

async def config_confirm(callback: types.CallbackQuery, state: FSMContext):
    await bot.edit_message_reply_markup(callback.from_user.id, message_id=callback.message.message_id,
                                        reply_markup=None)
    async with state.proxy() as data_config:
        if await just_db.sql_config_update(data_config['table'], data_config['collumn'], data_config['identificator'],
                                        data_config['value']):
            await send_message(callback.from_user.id, 'Changes have been written')
        else:
            await send_message(callback.from_user.id, 'Some error occured')
    await callback.answer()
    await state.finish()

async def config_decline(callback: types.CallbackQuery, state: FSMContext):
    await state.finish()
    await bot.edit_message_reply_markup(callback.from_user.id, message_id=callback.message.message_id,
                                        reply_markup=None)
    await send_message(callback.from_user.id, 'Exit done')

async def insert_curr(message: types.Message, state: FSMContext):
    if message.from_user.id in ID:
        if await state.get_state():
            await state.finish()
        _input = message.get_args().split()
        _id = message.from_user.id
        if len(_input) == 7:
            _input = [i.replace(',', '.') for i in _input]
            await FSM_config.insert.set()
            async with state.proxy() as data_insertion:
                data_insertion['1'] = _input
            await send_message(_id, await txt_check_insertion(*_input), config_insert_curr_kb)
        else:
            await send_message(_id, 'Type construction in this pattern:\n№ name name_to_parc rate min round_acc wallet')
    else:
        await send_message(message.from_user.id, txt_unkown_comm, None)

async def insert_confirm(callback: types.CallbackQuery, state: FSMContext):
    await bot.edit_message_reply_markup(callback.from_user.id, message_id=callback.message.message_id,
                                        reply_markup=None)
    async with state.proxy() as data_insertion:
        if await just_db.sql_config_add_curr(*data_insertion['1']):
            await send_message(callback.from_user.id, 'Currency has been added')
        else:
            await send_message(callback.from_user.id, 'Some error occured')
    await callback.answer()
    await state.finish()

async def insert_decline(callback: types.CallbackQuery, state: FSMContext):
    await state.finish()
    await bot.edit_message_reply_markup(callback.from_user.id, message_id=callback.message.message_id,
                                        reply_markup=None)
    await send_message(callback.from_user.id, 'Exit done')

def register_adclient_admin_config(dp: Dispatcher):
    dp.register_message_handler(config_start, commands='cchange', state='*')
    dp.register_message_handler(config_table, state=FSM_config.table)
    dp.register_message_handler(config_identificator, state=FSM_config.identificator)
    dp.register_message_handler(config_collumn, state=FSM_config.collumn)
    dp.register_message_handler(config_value, state=FSM_config.value)
    dp.register_message_handler(config_photo, content_types=['document', 'photo'], state=FSM_config.photo)
    dp.register_callback_query_handler(config_confirm, state=[FSM_config.value,FSM_config.photo], text='config_confirm')
    dp.register_callback_query_handler(config_decline, state=[FSM_config.value,FSM_config.photo], text='config_decline')

    dp.register_message_handler(insert_curr, commands='insert')
    dp.register_callback_query_handler(insert_confirm, state=FSM_config.insert, text='config_ins_con')
    dp.register_callback_query_handler(insert_decline, state=FSM_config.insert, text='config_ins_dec')