from create_bot import dp, bot
from aiogram import types, Dispatcher
from aiogram.dispatcher.filters.state import State, StatesGroup
from aiogram.dispatcher import FSMContext
from database import main_db, just_db
from datetime import datetime as dt, timedelta
from keyboards.admin_kb import choiceMenu
from message_handler import send_message
from id_admin import ID
from config import time_add_to_utc, check_amount, tm, ref_program, curr, deposit_num
from all_text import txt_approve_req, txt_decline_req, txt_deposit_from_refs, txt_unkown_comm
from random import choice

############ Get file of dudes #################
async def get_file(message: types.Message):
    if message.from_user.id in ID:
        with open("database/users.db", 'rb') as f:
            await message.answer_document(f)
        with open("database/users_2.db", 'rb') as f:
            await message.answer_document(f)
    else:
        await send_message(message.from_user.id, txt_unkown_comm, None)

############ Check requests #################
class FSMcheeeck(StatesGroup):
    mode = State()
    loop = State()
    choice = State()

async def check_requests_start(message: types.Message, state: FSMContext):
    if message.from_user.id in ID:
        if message.get_args() in ['dep', 'with']:
            if await state.get_state():
                await state.finish()
            if message.get_args() == 'dep':
                array_of_req = await main_db.sql_request_select_all('deposit', '0')
                async with state.proxy() as data_req:
                    data_req['mode'] = 'deposit'
            else:
                array_of_req = await main_db.sql_request_select_all('withdrawal', '0')
                async with state.proxy() as data_req:
                    data_req['mode'] = 'withdrawal'
            if array_of_req:
                await FSMcheeeck.mode.set()
                async with state.proxy() as data_req:
                    data_req['1'] = array_of_req
                await send_message(message.from_user.id, "<b>Type mode:</b>\n<i>1</i> - in ascending order\n<i>-1</i> - in descending")
            else:
                await send_message(message.from_user.id, 'There is no requests')
        else:
            await send_message(message.from_user.id, 'Wtf command. Add dep or with')
    else:
        await send_message(message.from_user.id, txt_unkown_comm, None)

async def check_requests_mode(message: types.Message, state: FSMContext):
    if message.text in ['1', '-1']:
        async with state.proxy() as data_req:
            if message.text == '-1':
                data_req['1'] = data_req['1'][::-1]
            await send_message(message.from_user.id, 'Check started')
        await check_requests_loop(message, state)
    else:
        await message.answer('Invalid mode. Type 1 or -1')

async def check_requests_loop(message: types.Message, state: FSMContext):
    async with state.proxy() as data_req:
        if data_req['1']:
            if (await main_db.sql_request_select(data_req['1'][0][0]))[9] == '0':
                await send_message(message.from_user.id, f'Total requests: {len(data_req["1"])}', None)
                col1 = ['id', 'username', 'time', 'currency', 'amount']
                _time = dt.strptime(data_req['1'][0][3], "%y.%m.%d %H:%M") + timedelta(hours=time_add_to_utc)
                _time = _time.strftime("%H:%M %d %b")
                _reqs = await just_db.sql_logs_reqs_count(data_req['1'][0][0])
                _reqs = [str(i) for i in _reqs]
                if data_req['mode'] == 'deposit':
                    repl = f'''
<b>{col1[0]}</b>: <code>{data_req['1'][0][0]}</code>
<b>{col1[1]}</b>: {data_req['1'][0][1]}
<b>{col1[2]}</b>: {_time}
<b>{col1[3]}</b>: {data_req['1'][0][4]}
<b>{col1[4]}</b>: {data_req['1'][0][5]}
<b>To wallet</b>: {data_req['1'][0][8]}
<b>Approved/declined requests:</b> {'/'.join(_reqs)}
'''
                else:
                    repl = f'''
<b>{col1[0]}</b>: <code>{data_req['1'][0][0]}</code>
<b>{col1[1]}</b>: {data_req['1'][0][1]}
<b>{col1[2]}</b>: {_time}
<b>{col1[3]}</b>: {data_req['1'][0][4]}
<b>{col1[4]}</b>: {data_req['1'][0][5]}
<b>To wallet</b>: <code>{data_req['1'][0][8]}</code>
<b>Approved/declined requests:</b> {'/'.join(_reqs)}
'''
                await send_message(message.from_user.id, repl, choiceMenu)
                await FSMcheeeck.choice.set()
            else:
                await send_message(message.from_user.id, 'List has ended from the different side')
                await FSMcheeeck.loop.set()
        else:
            await FSMcheeeck.loop.set()
            await send_message(message.from_user.id, 'There is no requests. Type /stop')
async def check_requests_choice(message: types.Message, state: FSMContext):
    if message.text in ['True', 'False', 'Pass']:
        async with state.proxy() as data_req:
            if message.text != 'Pass':
                await main_db.sql_preremove_request(data_req['1'][0][0], message.text)
                await send_message(message.from_user.id, 'Operation done', None)
            else:
                await send_message(message.from_user.id, 'User passed', None)
            del data_req['1'][0]
        await check_requests_loop(message, state)
    else:
        await message.answer('Type True or False or /stop')

async def check_requests_stop(message: types.Message, state: FSMContext):
    await state.finish()
    await send_message(message.from_user.id, 'Checking requests has ended\n/rite to write all changes')

async def check_requests_undo(message: types.Message, state: FSMContext):
    user_id = message.get_args()
    if type(user_id) == tuple:
        await send_message(message.from_user.id, 'There is no such user')
    elif user_id and await main_db.sql_request_select(user_id):
        await main_db.sql_request_undo(user_id)
        await send_message(message.from_user.id, 'Undo done', None)
        await check_requests_loop(message, state)
    else:
        await send_message(message.from_user.id, 'There is no such user')

async def check_requests_write(message: types.Message, state: FSMContext):
    if message.from_user.id in ID:
        await write_changes(message)
        await send_message(message.from_user.id, 'All changes have been written')
    else:
        await send_message(message.from_user.id, txt_unkown_comm, None)

async def write_changes(message: types.Message): # probably u can remove argument
    for user in await main_db.sql_request_select_all('*', 'False'):
        if user[7] == 'deposit':
            await just_db.sql_logs_add(await tm(1), user[0], 'decline_dep_cr', user[5], f'time={user[3]}')

        else:
            await just_db.sql_logs_add(await tm(1), user[0], 'decline_with', user[5],
                                       f'time={user[3]}, commission={user[6]}')
        await send_message(user[0], await txt_decline_req(user[7]))
    await main_db.sql_remove_requests_by_state('False') #delete that was declined

    deposniki = await main_db.sql_request_select_all('deposit', 'True')
    for user in deposniki:
        _amount = round((await curr(user[4])).value_get('rate') * user[5], 2)
        if await main_db.sql_find(user[0]): # проверка были ли уже в юзерe депозиты
            await main_db.sql_add_value(user[0], 'am_of_deps', 1)
            for i in ['active_dep', 'deposited', 'trader_profit']:
                await main_db.sql_add_value(user[0], i, _amount)
        else:
            await main_db.sql_main_add(user[0], user[1], 1, 0, _amount, _amount, 0, _amount, user[2])
        await main_db.sql_deposit_add(user[0], user[3], await check_amount(_amount, 'days'), user[4], _amount)
        _uniq_value_dep = await main_db.sql_deposit_get_last_dep_id() + choice(deposit_num)
        await income_from_refs(user[0], _amount, _uniq_value_dep)
        await just_db.sql_logs_add(await tm(1), user[0], 'confirm_dep_cr', _amount, f"in crypto: {user[5]} {user[4]}")
        await send_message(user[0], f'<i>Your request has been approved.</i>', None)
        await send_message(user[0], await txt_approve_req('deposit', _amount, uni_id=_uniq_value_dep), None)
        await main_db.sql_remove_request(user[0])

    draniki = await main_db.sql_request_select_all('withdrawal', 'True')
    for user in draniki:
        _value_to_usd = round(user[5]*(await curr(user[4])).value_get('rate'), 2)
        _comm = round(user[6], 2)
        for i in ['on_account', 'trader_profit']:
            await main_db.sql_add_value(user[0], i, -_value_to_usd)
        await main_db.sql_add_value(user[0], 'withdrawn', _value_to_usd)
        await main_db.sql_add_value(user[0], 'on_account', -_comm)
        await just_db.sql_logs_add(await tm(1), user[0], 'confirm_with', _value_to_usd,
                                   f'time={user[3]}, commission={user[6]}')
        await main_db.sql_with_add(user[0], await tm(0), user[4], user[5], user[8])
        await send_message(user[0], await txt_approve_req('withdraw', None, user[5], user[4]), None)
        await main_db.sql_remove_request(user[0])

def register_adclient_admin2(dp: Dispatcher):
    dp.register_message_handler(get_file, commands=['get_docs'])

    dp.register_message_handler(check_requests_write, commands='rite', state=None)
    dp.register_message_handler(check_requests_start, commands='check', state=None)
    dp.register_message_handler(check_requests_mode, state=FSMcheeeck.mode)
    dp.register_message_handler(check_requests_stop, commands='stop', state=[FSMcheeeck.choice, FSMcheeeck.loop])
    dp.register_message_handler(check_requests_undo, commands='undo', state=[FSMcheeeck.choice, FSMcheeeck.loop])
    dp.register_message_handler(check_requests_loop, state=FSMcheeeck.loop)
    dp.register_message_handler(check_requests_choice, state=FSMcheeeck.choice)

async def income_from_refs(id, amount, uniq_id):
    upline1 = (await main_db.sql_user_search(id))[1]
    if upline1:
        _amount = round(amount*ref_program[0]/100, 2)
        if not upline1 in await main_db.sql_bl_no_get():
            await send_message(upline1, await txt_deposit_from_refs(_amount, uniq_id), None)
        await main_db.sql_add_value(upline1, 'on_account', _amount)
        await main_db.sql_referral_add_value(upline1, 'profit_from_1', _amount)
        await just_db.sql_logs_add(await tm(1), upline1, 'funds_from_ref', _amount, f'lvl: 1, referral = {id}')
        up_line2 = (await main_db.sql_user_search(upline1))[1]
        if up_line2:
            _amount = round(amount * ref_program[1] / 100, 2)
            if not up_line2 in await main_db.sql_bl_no_get():
                await send_message(up_line2, await txt_deposit_from_refs(_amount, uniq_id), None)
            await main_db.sql_add_value(up_line2, 'on_account', _amount)
            await main_db.sql_referral_add_value(up_line2, 'profit_from_2', _amount)
            await just_db.sql_logs_add(await tm(1), up_line2, 'funds_from_ref', _amount, f'lvl: 2, referral = {id}')
            ine3 = (await main_db.sql_user_search(up_line2))[1]
            if ine3:
                _amount = round(amount * ref_program[2] / 100, 2)
                if not ine3 in await main_db.sql_bl_no_get():
                    await send_message(ine3, await txt_deposit_from_refs(_amount, uniq_id), None)
                await main_db.sql_add_value(ine3, 'on_account', _amount)
                await main_db.sql_referral_add_value(ine3, 'profit_from_3', _amount)
                await just_db.sql_logs_add(await tm(1), ine3, 'funds_from_ref', _amount, f'lvl: 3, referral = {id}')