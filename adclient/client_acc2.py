from aiogram import types, Dispatcher
from create_bot import dp, bot
from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters.state import State, StatesGroup
from aiogram.utils.exceptions import MessageNotModified
from aiogram.types import ParseMode
from all_text import txt_deps, txt_with_hist, txt_refs, txt_ref_the_same_page, txt_ref_link, command_switch_notif
from keyboards.client_kb import ref_lvls_kb, back_to_ac_kb
from message_handler import send_message
from database import main_db, just_db

class User_refs(StatesGroup):
    lvl = State()

async def active_deps(callback: types.CallbackQuery, state: FSMContext):
    if await state.get_state():
        await state.finish()
    await send_message(callback.from_user.id, await txt_deps(callback.from_user.id), back_to_ac_kb)
    await callback.answer()

async def with_hist(callback: types.CallbackQuery, state: FSMContext):
    if await state.get_state():
        await state.finish()
    await send_message(callback.from_user.id, await txt_with_hist(callback.from_user.id), back_to_ac_kb)
    await callback.answer()

async def refs(callback: types.CallbackQuery, state: FSMContext):
    await User_refs.lvl.set()
    user_id = callback.from_user.id
    file_id = await just_db.sql_config_values_get('config_photos', 'place', 'ref_prog', 'id')
    if file_id.startswith('A'):
        photo = True
    else:
        photo = False
    await callback.answer()
    if photo:
        await send_message(user_id, await txt_ref_link(user_id), back_to_ac_kb, photo=file_id)
    else:
        await send_message(user_id, await txt_ref_link(user_id), back_to_ac_kb, file=file_id)
    if await main_db.sql_referral_search(user_id):
        await send_message(user_id, f"In order to turn on/off notifications of referral investments type /{command_switch_notif}", None)
        await send_message(user_id, await txt_refs(user_id, 1), ref_lvls_kb)
    else:
        await send_message(user_id, 'You have no referrals. In order to get them send link to your friends.', None)

async def refs_lvl_1(callback: types.CallbackQuery, state: FSMContext):
    try:
        await bot.edit_message_text(await txt_refs(callback.from_user.id, 1), callback.message.chat.id,
                                    callback.message.message_id, parse_mode=ParseMode.HTML, reply_markup=ref_lvls_kb)
    except MessageNotModified:
        await callback.answer(txt_ref_the_same_page)
    else:
        await callback.answer()

async def refs_lvl_2(callback: types.CallbackQuery, state: FSMContext):
    try:
        await bot.edit_message_text(await txt_refs(callback.from_user.id, 2), callback.message.chat.id,
                                    callback.message.message_id, parse_mode=ParseMode.HTML, reply_markup=ref_lvls_kb)
    except MessageNotModified:
        await callback.answer(txt_ref_the_same_page)
    else:
        await callback.answer()

async def refs_lvl_3(callback: types.CallbackQuery, state: FSMContext):
    try:
        await bot.edit_message_text(await txt_refs(callback.from_user.id, 3), callback.message.chat.id,
                                    callback.message.message_id, parse_mode=ParseMode.HTML, reply_markup=ref_lvls_kb)
    except MessageNotModified:
        await callback.answer(txt_ref_the_same_page)
    else:
        await callback.answer()

async def switch_notif(message: types.Message, state: FSMContext):
    user_id = message.from_user.id
    await main_db.sql_user_update(user_id, 'notif_refs')
    await send_message(user_id, '<i>Notification has been updated.</i>', None)

def register_adclient_client_invest(dp: Dispatcher):
    dp.register_callback_query_handler(active_deps, state='*', text='deposits')
    dp.register_callback_query_handler(with_hist, state='*', text='with_hist')
    dp.register_callback_query_handler(refs, state='*', text='referals')
    dp.register_callback_query_handler(refs_lvl_1, state=User_refs.lvl, text='first_lvl')
    dp.register_callback_query_handler(refs_lvl_2, state=User_refs.lvl, text='second_lvl')
    dp.register_callback_query_handler(refs_lvl_3, state=User_refs.lvl, text='third_lvl')

    dp.register_message_handler(switch_notif, commands=command_switch_notif, state='*')