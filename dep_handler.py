from database import main_db, just_db
from datetime import datetime as dt, timedelta
import time
from config import deposits, check_amount, tm, curr
import random
import asyncio
from message_handler import send_message
from all_text import txt_accrual, txt_end_dep

async def main():
    print('Deposit handler was launched')
    while True:
        await accrual_of_funds()
        await asyncio.sleep(19)

async def main2():
    print("Changing perc's time was launched")
    while True:
        await asyncio.sleep(5)
        await up_date_percentage_rate()
        await asyncio.sleep(20)

async def accrual_of_funds():
    a = await main_db.sql_deposit_ids()
    for dep_id in a:
        values = await main_db.sql_deposit_get_values(dep_id)
        start_tm = dt.strptime(values[2], '%y.%m.%d %H:%M')
        tm_now = dt.utcnow()
        if start_tm.minute == tm_now.minute and start_tm.hour == tm_now.hour:
            print(tm_now)
            if tm_now.date() == (start_tm + timedelta(days=await check_amount(values[-1], 'days')
                                                                 - values[3] + 1)).date():
                _fund_to_acc = round(values[-1] * ((await just_db. \
                                                    sql_percs_get())[await check_amount(values[-1], 'id')]) / 100, 2)
                await main_db.sql_add_value(values[1], 'on_account', _fund_to_acc)
                await just_db.sql_logs_add(await tm(1), values[1], 'funds_from_dep', _fund_to_acc,
                                           f'dep_id = {values[0]}')
                await send_message(values[1], txt_accrual(_fund_to_acc, values[-1]), None)
                if await main_db.sql_update_days_by_one(values[0]) == 0:
                    await main_db.sql_add_value(values[1], 'am_of_deps', -1)
                    await main_db.sql_add_value(values[1], 'on_account', values[-1])
                    await main_db.sql_add_value(values[1], 'active_dep', -values[-1])
                    await just_db.sql_logs_add(await tm(1), values[1], 'end_of_dep', values[-1],
                                                                                            f'dep_id = {values[0]}')
                    await send_message(values[1], txt_end_dep(values[-1]), None)

async def up_date_percentage_rate():
    if dt.utcnow().hour == 5 and dt.utcnow().minute == 0:
        # await up_date_curr_rate() не работает
        await update_users_amount()
        for num, name in enumerate(just_db.plan_names):
            value = await get_value(num)
            await just_db.sql_perc_change(name, value)
            await just_db.sql_logs_add(await tm(1), None, 'set_percent_rate', value, f'plan = {name}')
        await asyncio.sleep(70)

async def up_date_curr_rate():
    from bs4 import BeautifulSoup as bs
    import requests
    headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.149 Safari/537.36'}
    base = f'https://minfin.com.ua/currency/crypto/'
    _currs = await just_db.sql_config_ids_get('currencies')
    currs = {}
    for i in _currs:
        _name = await just_db.sql_config_values_get('currencies', 'name', i, 'name_to_parc')
        if _name:
            currs[i] = _name
    for key in currs.keys():
        try:
            page = requests.get(base+currs[key], params=headers).text
            soup = bs(page, 'html.parser')
            value = soup.find('p', class_='mfm-coin--price').text.split() #должен быть другой класс, сайт переписали
        except (requests.exceptions.ConnectionError, AttributeError):
            pass
        else:
            if value:
                del value[-1]
                value = float((''.join(value)).replace(',', '.'))
                _round_acc = (await curr(key)).value_get('round_acc')
                min_usd = (await curr('usd')).value_get('min')
                _min = round(min_usd/value, _round_acc)
                await just_db.sql_config_update('currencies', 'rate', key, value)
                await just_db.sql_config_update('currencies', 'min', key, _min)

async def update_users_amount():
    get = await main_db.sql_amount_active()
    await just_db.sql_config_update('config', 'value', 'am_of_users', get)


async def get_value(num_plan):
    if num_plan == 0:
        _nw = random.uniform(0.5, 1.5)
        _nw = round(_nw, 3)
    elif num_plan == 1:
        _nw = random.uniform(1, 2)
        _nw = round(_nw, 3)
    elif num_plan == 2:
        _nw = random.uniform(2, 3)
        _nw = round(_nw, 3)
    else:
        _nw = random.uniform(3, 4)
        _nw = round(_nw, 3)
    return _nw