import sqlite3

from aiogram.utils import executor
from create_bot import dp
from database.main_db import sql_start, sql_close_1
from database.just_db import sql_start_2, sql_close_2
from dep_handler import main as main_dep_hand, main2 as main_perc_hand
from message_handler import ThrottlingMiddleware, AccessMiddleware
import asyncio

def on_startup():
    print('Бот вышел в онлайн')
    sql_start()
    sql_start_2()


async def on_shutdown(_):
    print('Закрытие баз данных и завершение работы')
    sql_close_1()
    sql_close_2()

from adclient import client, admin, other, admin2, client_acc, client_acc2, admin3, admin_config

admin.register_adclient_admin_cancel(dp) #here comes cancel
client.register_adclient_client(dp)
client_acc.register_adclient_client_invest(dp)
client_acc2.register_adclient_client_invest(dp)

admin.register_adclient_admin_update(dp)
admin.register_adclient_admin_search(dp)

admin2.register_adclient_admin2(dp)
admin3.register_adclient_admin3(dp)

admin_config.register_adclient_admin_config(dp)

other.register_adclient_other(dp)

if __name__ == '__main__':
    on_startup()
    loop = asyncio.get_event_loop()
    loop.create_task(main_dep_hand())
    loop.create_task(main_perc_hand())
    dp.middleware.setup(AccessMiddleware())
    dp.middleware.setup(ThrottlingMiddleware())
    executor.start_polling(dp, skip_updates=True, on_shutdown=on_shutdown)